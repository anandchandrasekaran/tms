const path    = require('path');
const express = require('express');
const app = new express();
const port = 5000;

app.use(express.static(path.join(__dirname)));
app.use('/', (request, response) => {
  response.sendFile(path.join(`${__dirname}/index.html`));
});

app.listen(port, err => {
  if (err) {
    console.log('Error in running application. Error: ', err);
  }
  console.log('Application started. Running on port: ', port);
});
