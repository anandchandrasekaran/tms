'use strict';

/* eslint-disable import/no-extraneous-dependencies */
const path = require('path');
const config = require('config');
const webpack = require('webpack');
// const Visualizer = require('webpack-visualizer-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

let _config = {};
if (process.env.API_ENDPOINT) {
  _config.baseApiUrl = process.env.API_ENDPOINT;
} else {
  _config = config.clientConfig;
}

module.exports = {
  mode: 'development',
  resolve: {
    extensions: ['.js', '.jsx', '.css', '.scss']
  },
  entry: [
    './src/js/app.jsx',
    'webpack-dev-server/client?http://0.0.0.0:8090', // WebpackDevServer host and port
    'webpack/hot/only-dev-server'
  ],
  output: {
    path: `${path.resolve(__dirname)}/dist`,
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        include: /(src[\/\\]js)/,
        loader: 'babel-loader'
      },
      {
        test: /\.jsx?$/,
        include: /(src[\/\\]js)/,
        loader: 'babel-loader'
      },
      {
        test: /\.json?$/,
        exclude: /(node_modules)/,
        loader: 'json-loader'
      },
      {
        test: /\.(scss|sass|css)$/,
        exclude: /node_modules/,
        loaders: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: "[local]___[hash:base64:5]"
              },
              importLoaders: 2,
              sourceMap: true
            }
          },
          'sass-loader'
        ]
      },
      {
        test: /\.(png|ico|gif|svg)?$/,
        loader: 'file-loader'
      }
    ]
  },
  devServer: {
    port: 8090,
    compress: true,
    contentBase: path.join(__dirname, 'dist')
  },
  plugins: [
    new CopyWebpackPlugin([
      { from: './assets', to: './assets' }
    ]),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css'
    }),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      inject: 'body',
      template: path.join(__dirname, './src/index.html')
    }),
    new webpack.IgnorePlugin(/^(buffertools)$/),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      ENV: JSON.stringify(config.webpack),
      appConfig: JSON.stringify(_config)
    }),
    // new Visualizer({ filename: '../reports/bundle-statistics.html' }),
    new webpack.LoaderOptionsPlugin({ debug: process.env === 'production' })
  ],
  devtool: 'source-map'
};
