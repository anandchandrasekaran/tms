'use strict';

// eslint-disable-next-line no-unused-vars
const stagingConfig = {
  baseApiUrl: 'http://15.207.81.123:8000'
};

module.exports = {
  clientConfig: {
    ...stagingConfig
  }
};
