'use strict';

// eslint-disable-next-line no-unused-vars
const prodConfig = {
  baseApiUrl: 'https://shazam-ocr-tag.madstreetden.com'
};

module.exports = {
  clientConfig: {
    ...prodConfig
  }
};
