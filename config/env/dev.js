'use strict';

const developConfig = {
  baseApiUrl: 'http://18.205.143.194:8000'
};

module.exports = {
  clientConfig: {
    ...developConfig
  }
};
