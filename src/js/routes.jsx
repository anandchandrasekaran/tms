import React from 'react';
import {
  Route,
  Switch,
  Redirect
} from 'react-router-dom';
import MainLayout from './layouts/mainLayout';
import LoginLayout from './layouts/loginLayout';
import SignupLayout from './layouts/signupLayout';
import VerifyUserLayout from './layouts/verifyUserLayout';

export default (
  <Route>
    <Switch>
      <Redirect from="/" to="/login" exact={true} />
      <Route path="/login" component={LoginLayout} exact={true} />
      <Route path="/signup" component={SignupLayout} exact={true} />
      <Route path="/verify" component={VerifyUserLayout} exact={true} />
      <Route path="*" component={MainLayout} exact={true} />
    </Switch>
  </Route>
);
