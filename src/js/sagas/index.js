import { all, takeLatest } from 'redux-saga/effects';
// custom imports
import { signin, refreshToken, logout, verifyUser } from '../containers/signin/sagas';
import {
  fetchProjects,
  createProject,
  disAssociateUser,
  get as getProject,
  fetchRoles as getRoles,
  updateProjectPermission,
  fetchUsers as fetchProjectUsers,
  fetchNonUsers as fetchNonProjectUsers
} from '../containers/projects/sagas';
import {
  signup,
  getUsers,
  mapUsers,
  inviteUser,
  getCurrentUser,
  get as getUser
} from '../containers/user/sagas';
import {
  markAsRead,
  fetch as fetchNotifications
} from '../containers/notifications/sagas';
import {
  createTag,
  fetchTags,
  detachTag,
  fetchTagData,
  createChildTag
} from '../containers/taxonomy/sagas';

const sagas = [
  // All root sagas needs to go in here
  [takeLatest, 'SESSION_USER:FETCH', getCurrentUser],
  [takeLatest, 'USER:FETCH', getUser],
  [takeLatest, 'USER:LOGOUT', logout],
  [takeLatest, 'USER:SIGN_IN', signin],
  [takeLatest, 'USER:SIGN_UP', signup],
  [takeLatest, 'MAP:USER', mapUsers],
  [takeLatest, 'INVITE:USER', inviteUser],
  [takeLatest, 'VERIFY:USER', verifyUser],
  [takeLatest, 'USER_LIST:FETCH', getUsers],
  [takeLatest, 'NOTIFICATIONS:FETCH', fetchNotifications],
  [takeLatest, 'NOTIFICATION:READ', markAsRead],
  [takeLatest, 'PROJECT:CREATE', createProject],
  [takeLatest, 'PROJECT:FETCH', getProject],
  [takeLatest, 'PROJECT_LIST:FETCH', fetchProjects],
  [takeLatest, 'PROJECT_USERS:FETCH', fetchProjectUsers],
  [takeLatest, 'PROJECT_NON_USERS:FETCH', fetchNonProjectUsers],
  [takeLatest, 'PROJECT_ROLES:FETCH', getRoles],
  [takeLatest, 'PROJECT_PERMISSION:UPDATE', updateProjectPermission],
  [takeLatest, 'DISASSOCIATE:USER', disAssociateUser],
  [takeLatest, 'TAG:CREATE', createTag],
  [takeLatest, 'TAGS:FETCH', fetchTags],
  [takeLatest, 'TAG:DELETE', detachTag],
  [takeLatest, 'CHILD_TAG:CREATE', createChildTag],
  [takeLatest, 'TAG_DATA:FETCH', fetchTagData],
  [takeLatest, 'AUTH:TOKEN_REFRESH', refreshToken]
];

function* rootSaga() {
  yield all([
    sagas.map(saga => function* () {
      yield saga[0](saga[1], saga[2]);
    }).map(saga => saga.call())
  ]);
}

export default rootSaga;
