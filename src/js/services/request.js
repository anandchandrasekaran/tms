/* eslint prefer-promise-reject-errors: ["error", {"allowEmptyReject": true}] */
import registry from 'app-registry';

let config = '';
const FAILED = 'FAILED';
const SUCCESS = 'SUCCESS';
const commonHeader = {
  Accept: 'application/json',
  'Content-Type': 'application/json'
};

/**
 * parses the given data
 *
 * @param      {<type>}  data    The data
 * @return     {<type>}  { description_of_the_return_value }
 */
function parse(data) {
  try {
    return JSON.parse(data);
  } catch (err) {
    return data;
  }
}

/**
 * Gets the default header.
 *
 * @return     {<type>}  The default header.
 */
function getDefaultHeader() {
  return JSON.parse(JSON.stringify(commonHeader));
}

/**
 * Gets the value by name.
 *
 * @param      {<type>}  key     The key
 * @return     {<type>}  The value by name.
 */
function getValueByName(key) {
  return registry.get(key);
}

/**
 * Formats the API response
 *
 * @param      {<type>}   response  The response
 * @return     {Promise}  { description_of_the_return_value }
 */
function transformResponse(response) {
  return new Promise(resolve => {
    response.text().then(res => {
      const status = parseInt(response.status, 10);
      const isSuccess = (status >= 200 && status < 300);
      const result = parse(res);
      resolve({
        isSuccess,
        body: result,
        response: result,
        type: isSuccess ? SUCCESS : FAILED
      });
    }).catch(err => {
      getValueByName('logger').error(err);
      resolve({ body: err, type: FAILED });
    });
  });
}

/**
 * Gets the request url.
 *
 * @param      {<type>}  url     The url
 * @return     {<type>}  The request url.
 */
function getRequestUrl(url) {
  if (!config) {
    config = getValueByName('config');
  }
  // Prepend domain to url
  return `${config.baseApiUrl}${url}`;
}

/**
 * Gets the auth token.
 *
 * @return     {<type>}  The auth token.
 */
function getAuthToken() {
  return getValueByName('storage').getItem('authToken');
}

/**
 * Gets the header.
 *
 * @param      {<type>}  url     The url
 * @return     {Object}  The header.
 */
function getHeader(method, url, data) {
  let headers = getDefaultHeader();
  // Attaching auth-token for each request
  const authToken = getAuthToken();
  if (authToken) {
    headers = Object.assign(headers, {
      Authorization: `Bearer ${authToken}`
    });
  }
  const requestPayload = { method: method.toUpperCase(), headers };
  switch (method) {
    case 'post':
    case 'put':
    case 'patch':
      requestPayload.body = JSON.stringify(data || {});
      break;
    case 'get':
    case 'delete':
      break;
    default:
      getValueByName('logger').error('No HTTP method matching');
  }
  return {
    headers,
    requestPayload,
    requestUrl: getRequestUrl(url)
  };
}

/**
 * Makes a request.
 *
 * @param      {Function}  method  The method
 * @param      {<type>}    url     The url
 * @param      {<type>}    data    The data
 * @return     {Promise}   { description_of_the_return_value }
 */
function makeRequest(method, url, data) {
  const { requestUrl, requestPayload } = getHeader(method, url, data);
  return new Promise(resolve => {
    fetch(requestUrl, requestPayload)
      .then(transformResponse)
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        getValueByName('logger').error(err);
        resolve(err);
      });
  });
}

/**
 * Gets the method.
 *
 * @param      {<type>}   url      The url
 * @param      {<type>}   options  The options
 * @return     {Promise}  The method.
 */
function getMethod(url) {
  return makeRequest('get', url);
}

/**
 * Posts a method.
 *
 * @param      {<type>}   url     The url
 * @param      {<type>}   data    The data
 * @return     {Promise}  { description_of_the_return_value }
 */
function postMethod(url, data) {
  return makeRequest('post', url, data);
}

/**
 * Puts a method.
 *
 * @param      {<type>}   url     The url
 * @param      {<type>}   data    The data
 * @return     {Promise}  { description_of_the_return_value }
 */
function putMethod(url, data) {
  return makeRequest('put', url, data);
}

/**
 * { function_description }
 *
 * @param      {<type>}   url     The url
 * @param      {<type>}   data    The data
 * @return     {Promise}  { description_of_the_return_value }
 */
function patchMethod(url, data) {
  return makeRequest('patch', url, data);
}

/**
 * { function_description }
 *
 * @param      {<type>}   url      The url
 * @param      {<type>}   options  The options
 * @return     {Promise}  { description_of_the_return_value }
 */
function deleteMethod(url) {
  return new Promise(resolve => {
    const xhr = new XMLHttpRequest();
    xhr.open('DELETE', getRequestUrl(url), true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.setRequestHeader('Authorization', `Bearer ${getAuthToken()}`);
    xhr.onload = () => {
      const response = JSON.parse(xhr.responseText);
      if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 201)) {
        resolve({ response, isSuccess: true });
      } else {
        resolve({ response, isSuccess: false });
      }
    };
    xhr.send(null);
  });
}

const request = {
  get: getMethod,
  put: putMethod,
  post: postMethod,
  patch: patchMethod,
  delete: deleteMethod
};

registry.register('request', request);

export { request as default };
