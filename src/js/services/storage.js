import registry from 'app-registry';

function setItemMethod(key, value) {
  return localStorage.setItem(key, value);
}

function getItemMethod(key) {
  return localStorage.getItem(key);
}

function removeItemMethod(key) {
  return localStorage.removeItem(key);
}

function clearAllItemsMethod() {
  return localStorage.clear();
}

const storage = {
  setItem: setItemMethod,
  getItem: getItemMethod,
  removeItem: removeItemMethod,
  clearAllItems: clearAllItemsMethod
};
registry.register('storage', storage);
export { storage as default };
