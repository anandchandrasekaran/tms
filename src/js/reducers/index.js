import user from '../containers/user/reducers';
import loader from '../containers/loader/reducers';
import signin from '../containers/signin/reducers';
import taxonomy from '../containers/taxonomy/reducers';
import notifier from '../containers/notifier/reducers';
import projects from '../containers/projects/reducers';
import notifications from '../containers/notifications/reducers';

export default { projects, loader, notifier, signin, user, taxonomy, notifications };
