import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import styles from './main.scss';
import SignUp from '../containers/signup';
import Loader from '../containers/loader';
import Notifier from '../containers/notifier';

class SignupLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className={styles.main}>
        <Loader />
        <Notifier />
        <Switch>
          <Route path="/signup" component={SignUp} exact={true} />
        </Switch>
      </div>
    );
  }
}

SignupLayout.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string
  }).isRequired
};

export default SignupLayout;
