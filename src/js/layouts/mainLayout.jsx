import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch, Redirect } from 'react-router-dom';
import { authVerify, onEnterRouterHook } from '../utils/routerHooks';

import styles from './main.scss';
import Users from '../containers/user';
import Header from '../containers/header';
import Loader from '../containers/loader';
import Notifier from '../containers/notifier';
import Projects from '../containers/projects';
import Taxonomy from '../containers/taxonomy';
import UserMapping from '../containers/usermapping';
import Notifications from '../containers/notifications';

class MainLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    onEnterRouterHook(this.props);
  }

  render() {
    return (
      <div className={styles.main}>
        <div className={styles.home}>
          {authVerify() ?
            <React.Fragment>
              <Header />
              <div className={styles.content}>
                <div className={styles.main_content}>
                  <Loader />
                  <Notifier />
                  <div className={styles.mcontent}>
                    <Switch>
                      <Redirect from="/" to="/home" exact={true} />
                      <Route path="/home" component={Projects} exact={true} />
                      <Route path="/project" component={Projects} exact={true} />
                      <Route path="/project/:projectId" component={UserMapping} exact={true} />
                      <Route path="/project/:projectId/taxonomy" component={Taxonomy} exact={true} />
                      <Route path="/user" component={Users} exact={true} />
                      <Route path="/mapping" component={UserMapping} exact={true} />
                      <Route path="/notifications" component={Notifications} exact={true} />
                    </Switch>
                  </div>
                </div>
              </div>
            </React.Fragment> : ''
          }
        </div>
      </div>
    );
  }
}

MainLayout.contextTypes = {
  router: PropTypes.shape({}).isRequired
};

export default MainLayout;
