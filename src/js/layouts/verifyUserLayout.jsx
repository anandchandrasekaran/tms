import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import styles from './main.scss';
import Loader from '../containers/loader';
import Notifier from '../containers/notifier';
import { checkUserSession } from '../utils/routerHooks';
import ConfirmAccount from '../containers/confirmaccount';

class VerifyUserLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    if (this.props.location.pathname === '/verify') {
      checkUserSession();
    }
  }

  render() {
    return (
      <div className={styles.main}>
        <Loader />
        <Notifier />
        <Switch>
          <Route path="/verify" component={ConfirmAccount} exact={true} />
        </Switch>
      </div>
    );
  }
}

VerifyUserLayout.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string
  }).isRequired
};

export default VerifyUserLayout;
