import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import styles from './main.scss';
import SignIn from '../containers/signin';
import Loader from '../containers/loader';
import Notifier from '../containers/notifier';
import { onEnterRouterHook } from '../utils/routerHooks';

class LoginLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    if (this.props.location.pathname === '/login') {
      onEnterRouterHook(this.props, true);
    }
  }

  render() {
    return (
      <div className={styles.main}>
        <Loader />
        <Notifier />
        <Switch>
          <Route path="/login" component={SignIn} exact={true} />
        </Switch>
      </div>
    );
  }
}

LoginLayout.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string
  }).isRequired
};

export default LoginLayout;
