import registry from 'app-registry';

/**
 * { function_description }
 */
export function noFunc() {}

/**
 * isEmpty
 *
 * @param      {String|Number|Object}  value   The value
 * @return     {Boolean}  Validates the given value and returns the boolean value as result
 */
export function isEmpty(value) {
  return value === '' || value === undefined || value === null || typeof value === 'undefined';
}

/**
 * Determines if array.
 *
 * @param      {<type>}   data    The data
 * @return     {boolean}  True if array, False otherwise.
 */
export function isArray(data) {
  return Object.prototype.toString.call(data) === '[object Array]';
}
/**
 * Determines if object.
 *
 * @param      {<type>}   data    The data
 * @return     {boolean}  True if object, False otherwise.
 */
export function isObject(data) {
  return Object.prototype.toString.call(data) === '[object Object]';
}
/**
 * Determines if string.
 *
 * @param      {<type>}   data    The data
 * @return     {boolean}  True if string, False otherwise.
 */
export function isString(data) {
  return Object.prototype.toString.call(data) === '[object String]';
}
/**
 * validateEmail
 *
 * @param      {String}  value   The value
 * @return     {Boolean}  Validates the given string as valid email pattern and returns the boolean value
 */
export function validateEmail(value) {
  // Let's not start a debate on email regex. This is just for an example app!
  return !isEmpty(value) && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i.test(value);
}

/**
 * { function_description }
 *
 * @param      {<type>}  data    The data
 * @return     {<type>}  { description_of_the_return_value }
 */
export function formatMessage(data) {
  return (data && data.message) ? data.message : '';
}

/**
 * { function_description }
 *
 * @return     {<type>}  { description_of_the_return_value }
 */
export function request() {
  return registry.get('request');
}

/**
 * { function_description }
 *
 * @return     {<type>}  { description_of_the_return_value }
 */
export function store() {
  return registry.get('store');
}

/**
 * { function_description }
 *
 * @return     {<type>}  { description_of_the_return_value }
 */
export function storage() {
  return registry.get('storage');
}

/**
 * convertToObject
 *
 * @param      {<type>}   key          The key
 * @param      {<type>}   list         The list
 * @param      {boolean}  isLowerCase  Indicates if lower case
 * @return     {<type>}   { description_of_the_return_value }
 */
export function convertToObject(key, list, isLowerCase) {
  const result = {};
  if (list && list.length > 0 && isArray(list)) {
    if (isLowerCase) {
      list.forEach(obj => {
        result[(obj[key]).toLowerCase()] = obj;
      });
    } else {
      list.forEach(obj => {
        result[obj[key]] = obj;
      });
    }
  }
  return result;
}
