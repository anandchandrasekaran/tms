import registry from 'app-registry';

function getAuthToken() {
  return registry.get('storage').getItem('authToken');
}

/**
 * { function_description }
 *
 * @return     {<type>}  { description_of_the_return_value }
 */
export function authVerify() {
  return getAuthToken() !== null;
}

/**
 * Gets the store.
 *
 * @return     {<type>}  The store.
 */
function getStore() {
  return registry.get('store');
}

/**
 * { function_description }
 *
 * @param      {boolean}  isFromLogin  Indicates if from login
 */
function refreshToken(isFromLogin) {
  const store = getStore();
  const action = { type: 'AUTH:TOKEN_REFRESH' };
  if (isFromLogin) {
    action.isFromLogin = isFromLogin;
  }
  store.dispatch(action);
}

/**
 * checks the user session
 */
export function checkUserSession() {
  if (getAuthToken()) {
    refreshToken(null);
  }
}

/**
 * Called on enter router hook.
 *
 * @param      {<type>}   params       The parameters
 * @param      {boolean}  isFromLogin  Indicates if from login
 */
export function onEnterRouterHook(params, isFromLogin) {
  const authToken = getAuthToken();
  if (!authToken) {
    params.history.replace({ pathname: '/login' });
  } else if (params.location.pathname !== '/login' ||
    (authToken && params.location.pathname === '/login')) {
    refreshToken(isFromLogin);
  }
}

/**
 * Extracts the query params from the URL
 *
 * @return     {<type>}  { description_of_the_return_value }
 */
export function extractQueryParams() {
  // simple logic to extract query params from the url
  let result = {};
  try {
    const { hash } = window.location;
    if (hash) {
      const tempString = hash.split('?');
      if (tempString && tempString.length > 1) {
        let temp = tempString[1];
        temp = temp.split('&');
        if (temp && temp.length > 0) {
          temp.forEach(string => {
            const strArr = string.split('=');
            if (strArr && strArr.length === 2) {
              const key = strArr[0];
              const value = strArr[1];
              result[key] = value;
            }
          });
        }
      }
    }
  } catch (err) {
    // console.log('Error in extracting the query params from the URL');
    result = {};
  }
  return result;
}

/**
 * Gets the parameter values from hash.
 *
 * @param      {<type>}  url     The url
 * @return     {<type>}  The parameter values from hash.
 */
export function getParameterValuesFromHash(url) {
  if (url) {
    let { hash } = window.location;
    hash = hash.indexOf('?') > 0 ? hash.substring(0, hash.indexOf('?')) : hash;
    const parameterValues = {};
    const splitUrl = url.split('/');
    const splitHash = hash.split('/');

    for (let index = 1; index < splitUrl.length; index += 1) {
      let parameter = splitUrl[index];
      if (parameter.indexOf(':') >= 0) {
        parameter = parameter.substring(1);
        parameterValues[parameter] = splitHash[index];
      }
    }
    return parameterValues;
  }
  return null;
}
