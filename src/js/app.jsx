import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import registry from 'app-registry';
import { Provider } from 'react-redux';
import { HashRouter as Router } from 'react-router-dom';

import routes from './routes';
import { store } from './store';
// The following modules registered when imported
import logger from './services/logger';
import storage from './services/storage';
import request from './services/request';

registry.register('store', store);

/* eslint-disable no-undef */
if (typeof appConfig !== 'undefined') {
  const config = appConfig || {};
  registry.register('config', config);
  if (config.logger && config.logger.level) {
    logger.setLevel(config.logger.level);
  }
} else {
  registry.get('logger').warning('WARNING: The app config is not defined');
}

/* eslint-enable no-undef */
store.dispatch({ type: 'APP:INIT' });

ReactDOM.render(
  <Provider store={store}>
    <Router>
      {routes}
    </Router>
  </Provider>, document.getElementById('taxonomy_app')
);
