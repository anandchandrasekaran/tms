import React from 'react';
import styles from './style.scss';

const Banner = () => (
  <div className={styles.banner}>
    <div className={styles.image} />
  </div>
);

Banner.propTypes = {};

Banner.defaultProps = {};

export default Banner;
