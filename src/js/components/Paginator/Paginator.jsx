import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Paginator.scss';

class Paginator extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.ismounted = true;
    // The left and right arraow shortcuts will work, only when keyboard shortcuts enabled
    const { keyboard } = this.props.params;
    if (keyboard) {
      // Keyboard shortcuts for pagination(Binding the listener)
      document.addEventListener('keyup', this.handleNavigation);
    }
  }

  componentWillUnmount() {
    this.ismounted = false;
    // Keyboard shortcuts for pagination(remove the listener)
    document.removeEventListener('keyup', this.handleNavigation);
  }

  /**
   * `getPaginationMessage`
   * Construct the info of how many records are shown out of total records
   */
  getPaginationMessage = (page, limit, total) => {
    let start = 1;
    let end = page * limit;
    if (page > 1) {
      start = ((page * limit) - limit) + 1;
    }
    if (total <= end) {
      end = total;
    }
    return `Viewing ${start} - ${end} of ${total} records`;
  };

  /**
   * `handleNavigation`
   * It handles the keyboard events with corresponding action
   * Left arrow: 37, Right arrow: 39
   * On left arrow press, load previous page data
   * On next arrow press, load next page data
   */
  handleNavigation = event => {
    const keyCode = event.keyCode || event.which;
    if ([37, 39].indexOf(keyCode) !== -1 && this.ismounted) {
      const { params } = this.props;
      const { limit, page, total } = params;
      const max = Math.ceil(total / limit);
      if (keyCode === 37 && page > 1) {
        // on left arrow keyup, load prev page records
        params.goTo(page - 1);
      } else if (keyCode === 39 && page < max) {
        // on right arrow keyup, load next page records
        params.goTo(page + 1);
      }
    }
  };

  /**
   * `renderPageLimits`
   * Static page limits(multiples of 24)
   */
  renderPageLimits = () => (
    [10, 25, 50, 100].map(limit => (
      <option key={limit} value={limit} title={limit}>{limit}</option>
    ))
  );

  render() {
    const { params } = this.props;
    const { limit, page, total, resize } = params;
    /*
      limit - page limit (24, 48, ...)
      page - current page number (1, 2, 3, ...)
      total - total no.of records (123, 143, ...)
      selected - currently selected tab filter name (pending, reviewed, ...)
    */
    let max = 0;
    let skip = 0;
    let maxVisible = 0;
    // max - Maximum page upto that users can navigate.
    // i.e limit = 24, total = 66, then max = 3. So now user can navigate upto 3 pages
    max = Math.ceil(total / limit);
    let currentPage = page;
    if (max < currentPage) {
      currentPage = max;
    }
    // maxVisible - Maximum no.of pagination cell(s) that can be displayed in UI. Currently, set to display `5 cell`.
    const tempMaxVisible = resize ? 3 : 5;
    maxVisible = max < tempMaxVisible ? max : tempMaxVisible;
    // `skip` is used to generate pagination cell array based on following condition
    if (currentPage === max) {
      skip = currentPage - maxVisible;
    } else if ((currentPage < max) && (currentPage > (maxVisible - 1))) {
      skip = (currentPage - maxVisible) + 1;
    }
    // Generating pagination cell array
    const iterator = [];
    for (let index = 0; index < maxVisible;) {
      iterator[index] = skip + index + 1;
      index += 1;
    }

    return (
      <div
        style={params.style}
        className={`${styles.pagination} ${params.show ? '' : 'hide'} color-1`}
      >
        {/* Page limits */}
        <div className={styles.limit}>
          Showing
          <select
            data-size="5"
            id="pagelimit"
            title="Page limit"
            data-width="100px"
            onChange={params.loadData}
            data-dropdown-align-right="true"
            className="selectpicker show-tick dropup"
          >
            {this.renderPageLimits()}
          </select>
          <span title="records per page">records per page</span>
        </div>
        {/* Pagination bar */}
        <div className={`${styles.bar} ${parseInt(total, 10) <= limit ? 'disabled' : ''}`}>
          <nav className="color-4">
            <ul>
              {/* When currently selected page = 1, then disable left navigate cell */}
              <li
                role="presentation"
                onClick={() => { params.goTo(page - 1); }}
                className={page === 1 ? 'disabled' : ''}
              >
                <span aria-hidden="true">
                  <i className="fa fa-angle-left f-10" />
                </span>
              </li>
              {iterator[iterator.length - 1] === max && max > maxVisible &&
                <li role="presentation" onClick={() => { params.goTo(1); }}>
                  1
                </li>
              }
              {iterator[iterator.length - 1] === max && max > maxVisible &&
                <li style={{ cursor: 'default' }}>
                  ...
                </li>
              }
              {/* Rendering pagination cell array */}
              {iterator.map(pageNo => (
                <li
                  key={pageNo}
                  role="presentation"
                  onClick={() => { params.goTo(pageNo); }}
                  className={page === pageNo ? styles.active : ''}
                >
                  {pageNo}
                </li>
              ))
              }
              {iterator[iterator.length - 1] !== max &&
                <li style={{ cursor: 'default' }}>
                  ...
                </li>
              }
              {iterator[iterator.length - 1] !== max &&
                <li role="presentation" onClick={() => { params.goTo(max); }}>
                  {max}
                </li>
              }
              {/* When currently selected page reaches end, then disable right navigate cell */}
              <li
                role="presentation"
                onClick={() => { params.goTo(page + 1); }}
                className={page === max ? 'disabled' : ''}
              >
                <span aria-hidden="true">
                  <i className="fa fa-angle-right f-10" />
                </span>
              </li>
            </ul>
          </nav>
        </div>
        {/* No.of records showing message */}
        <div className={styles.message}>
          {this.getPaginationMessage(page, limit, total)}
        </div>
      </div>
    );
  }
}

Paginator.propTypes = {
  params: PropTypes.oneOfType([
    PropTypes.object
  ])
};

Paginator.defaultProps = {
  params: null
};

export default Paginator;
