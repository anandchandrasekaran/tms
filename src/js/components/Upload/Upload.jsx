import jquery from 'jquery';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody } from 'msd-modal';

import styles from './Upload.scss';

const staticEndPoint = 'https://static-dev.vuetag.ai/assets';
const close = `${staticEndPoint}/images/close.svg`;
const warningImages = `${staticEndPoint}/images/Warning_images.svg`;
const uploading = `${staticEndPoint}/images/upload_process_icon.svg`;
const infiniteLoader = `${staticEndPoint}/images/infinite_loader.svg`;
const uploadFailed = `${staticEndPoint}/images/Error_upload_icon.svg`;
const uploadSuccess = `${staticEndPoint}/images/success_upload_icon.svg`;
const withMetaData = `${staticEndPoint}/images/metadata_upload_icon.svg`;
const uploadValidation = `${staticEndPoint}/images/validating_upload_icon.svg`;

class Upload extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  getFileSize = fileSize => {
    let formattedSize = parseFloat(fileSize / 1024);
    if (formattedSize > 1023) {
      formattedSize = `${parseFloat(formattedSize / 1024).toFixed(2)} MB`;
    } else {
      formattedSize = `${parseFloat(formattedSize).toFixed(2)} KB`;
    }
    return formattedSize;
  };

  render() {
    const {
      fileName,
      fileSize,
      errorMsg,
      successMsg,
      uploadFile,
      warningMsg,
      handleHide,
      resetUpload,
      cancelUpload,
      uploadStatus,
      uploadPercent,
      showUploadModal
    } = this.props.params;
    const { isUploading, isSuccess, isFailed, isUploadError, isValidating } = uploadStatus;
    return (
      <Modal
        tag="div"
        showCloseIcon={false}
        className={styles.upload}
        isOpen={showUploadModal}
      >
        <ModalHeader
          tag="div"
          className={styles.header}
          style={{
            backgroundColor: (isFailed || isUploadError) ? '#FFFFFF' : '',
            borderBottom: (isFailed || isUploadError) ? '1px solid rgba(146, 156, 170, 0.2)' : '',
            visibility: ((isSuccess) || isValidating || isUploading) ? 'hidden' : 'visible'
          }}
        >
          <span className={styles.title}>
            {`Upload ${(isFailed || isUploadError) ? 'Error' : 'File'}`}
          </span>
          <div
            role="presentation"
            onClick={handleHide}
            className={`${styles.close} ${(isUploading || (isSuccess)) ? 'hide' : 'show'}`}
          >
            <img src={close} alt="close" />
          </div>
        </ModalHeader>
        <ModalBody
          tag="div"
          className={styles.content}
          style={{
            borderBottom: (
              isSuccess || isValidating || isUploading || isFailed || isUploadError
            ) ? 'none' : '',
            backgroundColor: (
              isSuccess || isValidating || isUploading || isFailed || isUploadError
            ) ? '#FFFFFF' : ''
          }}
        >
          <section>
            <div className={styles.common}>
              {/* UI code which handles the UI before upload starts */}
              <div
                className={
                  (
                    !isUploading && !isFailed && !isSuccess && !isUploadError && !isValidating
                  ) ? styles.uploads : 'hide'
                }
              >
                <div className={styles.meta}>
                  <div className={styles.image}>
                    <img data-meta="true" src={withMetaData} alt="withMetaData" />
                  </div>
                  <div className={styles.desc}>
                    <p>Upload File</p>
                    <p>
                      <span className="font-wt-600" data-imp="back">.TTL</span>
                      &nbsp;
                      or
                      &nbsp;
                      <span className="font-wt-600" data-imp="back">.CSV</span>
                    </p>
                  </div>
                  <div className={styles.input}>
                    <input
                      type="file"
                      id="uploadfile"
                      className="hide"
                      accept=".csv, .ttl"
                      onChange={uploadFile}
                    />
                    <button
                      type="button"
                      className="btn btn-primary font-wt-600"
                      onClick={() => {
                        jquery('#uploadfile').click();
                      }}
                    >
                      Upload CSV
                    </button>
                  </div>
                </div>
              </div>
              {isUploading &&
                <div className={`${styles.u_progress} ${isUploading ? 'show' : 'hide'}`}>
                  <div className={styles.image}>
                    <img src={uploading} alt="uploadingImage" />
                  </div>
                  <div className={styles.desc}>
                    <p>Uploading your file</p>
                    <p>
                      Hang on! Upload time will vary based on the file size
                    </p>
                  </div>
                  <div className={styles.info}>
                    <div className={styles.sec}>
                      <div className={styles.fname}>
                        <p>{fileName}</p>
                        {/* To show the file upload progress in terms of number(s). For example, 18MB of 50MB */}
                        <p>
                          {`${this.getFileSize(((uploadPercent / 100) * fileSize))} of ${this.getFileSize(fileSize)}`}
                        </p>
                      </div>
                      {/* To show the file upload progress in terms of percentage. For example, 68% completed */}
                      <div className={styles.uploadpercent}>
                        {`${uploadPercent}% Completed`}
                        {/* The followng div is used to cancel the current upload before it reaches 100% */}
                        <div
                          role="presentation"
                          className={styles.close}
                          onClick={cancelUpload}
                        >
                          <img src={close} alt="close" />
                        </div>
                      </div>
                    </div>
                    {/* To show the file upload progress in terms of progress bar */}
                    <div className={`progress ${styles.progressbar}`}>
                      <div
                        aria-valuemin="0"
                        role="progressbar"
                        aria-valuemax="100"
                        className="progress-bar"
                        aria-valuenow={uploadPercent}
                        style={{ width: `${uploadPercent}%` }}
                      />
                    </div>
                  </div>
                </div>
              }
              {isValidating &&
                <div className={`${styles.validate} ${isValidating ? 'show' : 'hide'}`}>
                  <div className={styles.image}>
                    <img src={uploadValidation} alt="upload file validation" />
                  </div>
                  <div className={styles.desc}>
                    <div>
                      <p>
                        Validating the uploaded file
                      </p>
                      <p className="color-4 font-16 text-center font-wt-600">
                        Please wait while we validate your uploaded file
                      </p>
                    </div>
                  </div>
                  <div className={styles.loader}>
                    <img src={infiniteLoader} alt="infinite loader" />
                  </div>
                </div>
              }
              {isFailed &&
                <div className={`${styles.failed} ${isFailed ? 'show' : 'hide'}`}>
                  <div className={styles.image}>
                    <img src={uploadFailed} alt="uploadFailed" />
                  </div>
                  <div className={styles.desc}>
                    <p>Failed to upload</p>
                    <p>
                      {errorMsg && errorMsg.message}
                    </p>
                  </div>
                  <div className="text-center">
                    <p className="text-center hide">
                      {`${fileName} ${this.getFileSize(fileSize)}`}
                    </p>
                    <button
                      type="button"
                      onClick={resetUpload}
                      className="btn btn-primary font-wt-600"
                    >
                      Re-Upload File
                    </button>
                  </div>
                </div>
              }
              {isUploadError &&
                <div className={`${styles.error} ${isUploadError ? 'show' : 'hide'}`}>
                  <div className={styles.image}>
                    <img src={warningImages} alt="uploadFailed" />
                  </div>
                  <div className={styles.desc}>
                    <p>{warningMsg && warningMsg.message}</p>
                    <strong>Do you still wish to continue tagging images?</strong>
                    <p>
                      {warningMsg && warningMsg.details &&
                        warningMsg.details.map(detail => (
                          <span key={Math.random()}>
                            {warningMsg.details.length > 1 && '⚫'}
                            &nbsp;
                            {detail}
                          </span>
                        ))
                      }
                    </p>
                  </div>
                  <div className={styles.info}>
                    <span className="color-2 font-wt-600">{fileName}</span>
                    <span className="float-right color-4 font-wt-600">{this.getFileSize(fileSize)}</span>
                  </div>
                  <div className={styles.action}>
                    <button
                      type="button"
                      onClick={resetUpload}
                      className="btn btn-default font-16"
                    >
                      Cancel
                    </button>
                    <button
                      type="button"
                      onClick={uploadFile}
                      className="btn btn-primary font-16"
                    >
                      Continue
                    </button>
                  </div>
                </div>
              }
              {isSuccess &&
                <div className={`${styles.success} ${isSuccess ? 'show' : 'hide'}`}>
                  <div className={styles.image}>
                    <img src={uploadSuccess} alt="uploadSuccess" />
                  </div>
                  <div className={styles.desc}>
                    <p>File Successfully Uploaded!</p>
                    <p className="font-16 color-4 text-center">
                      {successMsg || 'We are generating the tags for the images uploaded'}
                    </p>
                    <p className="font-wt-600 font-16 text-center">
                      {fileName}
                    </p>
                  </div>
                </div>
              }
            </div>
          </section>
        </ModalBody>
      </Modal>
    );
  }
}

Upload.propTypes = {
  params: PropTypes.oneOfType([
    PropTypes.object
  ])
};

Upload.defaultProps = {
  params: null
};

export default Upload;
