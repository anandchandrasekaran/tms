import React from 'react';
import PropTypes from 'prop-types';

import styles from './Breadcrumb.scss';
import { isEmpty } from '../../utils/common';

const Breadcrumb = ({ params }) => (
  <nav aria-label="breadcrumb" className={styles.breadcrumb}>
    <ol className="breadcrumb">
      {params.list && (params.list.length > 0) && params.list.map(listItem => (
        <li
          role="presentation"
          key={Math.random()}
          className="breadcrumb-item"
          onClick={() => {
            if (!isEmpty(listItem.link)) {
              params.push(listItem.link);
            }
          }}
        >
          <span>
            {listItem.name}
          </span>
        </li>
      ))
      }
    </ol>
  </nav>
);

Breadcrumb.propTypes = {
  params: PropTypes.oneOfType([
    PropTypes.object
  ])
};

Breadcrumb.defaultProps = {
  params: null
};

export default Breadcrumb;
