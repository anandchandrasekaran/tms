import { connect } from 'react-redux';
import Loader from './loader';

const mapStateToProps = state => ({
  show: state.loader.show
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Loader);
