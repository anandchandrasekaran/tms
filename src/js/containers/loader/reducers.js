import Immutable from 'seamless-immutable';

const defaultState = Immutable.flatMap({ show: false });

function mergeData(state, data) {
  return Immutable.merge(state, data);
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'LOADER:SHOW':
      return mergeData(state, { show: true });
    case 'LOADER:HIDE':
      return mergeData(state, { show: false });
    default:
      return state;
  }
};
