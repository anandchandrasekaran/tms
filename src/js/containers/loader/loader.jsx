import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './style.scss';

class Loader extends Component {
  render() {
    const { show } = this.props;
    return (
      <div className={`${styles.loading} ${show ? '' : 'hide'}`}>Loading&#8230;</div>
    );
  }
}

Loader.propTypes = {
  show: PropTypes.bool
};

Loader.defaultProps = {
  show: false
};

export default Loader;
