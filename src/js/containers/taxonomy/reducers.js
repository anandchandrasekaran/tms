import Immutable from 'seamless-immutable';

const defaultState = Immutable.flatMap({
  tags: [],
  newTag: {},
  tagData: {}
});

function mergeData(state, data) {
  return Immutable.merge(state, data);
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'TAGS:FETCH:SUCCESS':
      return mergeData(state, { tags: action.data });

    case 'TAG_DATA:FETCH:SUCCESS':
      return mergeData(state, { tagData: action.data });

    case 'CHILD_TAG:CREATE:SUCCESS':
      return mergeData(state, { childTag: action.data });

    case 'TAG:CREATE:SUCCESS':
      return mergeData(state, { newTag: action.data });

    case 'TAXONOMY:RESET':
      return mergeData(state, { tags: [], newTag: {}, tagData: {} });

    default:
      return state;
  }
};
