import { connect } from 'react-redux';
import Taxonomy from './taxonomy';

const mapStateToProps = state => ({
  selectedProject: state.projects.project
});

const mapDispatchToProps = dispatch => ({
  notify: ({ type, message }) => {
    dispatch({ type: 'NOTIFIER:SHOW', notification: { type, message } });
  },
  toggleLoader: show => {
    dispatch({ type: `LOADER:${show ? 'SHOW' : 'HIDE'}` });
  },
  fetchCurrentProject: projectId => {
    dispatch({ type: 'PROJECT:FETCH', projectId });
  },
  refreshNotifications: () => {
    dispatch({ type: 'NOTIFICATIONS:FETCH' });
  },
  resetValues: () => {
    dispatch({ type: 'TAXONOMY:RESET' });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Taxonomy);
