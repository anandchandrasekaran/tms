import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, ModalHeader, ModalBody } from 'msd-modal';
// custom components
import merge from './merge.png';
import styles from './style.scss';
import { isEmpty } from '../../../../utils/common';

const close = 'https://static-dev.vuetag.ai/assets/images/close.svg';

class AdvancedSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  closeModal = () => {
    this.props.params.handleHide(false);
  };

  addCriteria = () => {
    this.props.params.addCriteria();
  };

  listenFieldChange = (index, selector, value) => {
    this.props.params.listenChange(index, selector, value);
  };

  render() {
    const { params } = this.props;
    const { querySet, criteria, remove } = params;
    const { columns, comparators } = querySet;
    return (
      <Modal
        tag="div"
        showCloseIcon={false}
        className={styles.upload}
        isOpen={params.showAdvancedSearch}
      >
        <ModalHeader
          tag="div"
          className={styles.header}
        >
          <span className={styles.title}>Advanced Search</span>
          <div
            role="presentation"
            className={styles.close}
            onClick={this.closeModal}
          >
            <img src={close} alt="close" />
          </div>
        </ModalHeader>
        <ModalBody tag="div" className={styles.content}>
          <section>
            <div className={styles.common}>
              {/* UI code which handles the UI before upload starts */}
              <div className={styles.create}>
                <div>
                  {criteria && criteria.map((condition, index) => (
                    <div key={Math.random()} className={styles.row}>
                      <div
                        className={styles.logical}
                        style={{ visibility: (index > 0) ? 'visible' : 'hidden' }}
                      >
                        <select
                          id={`criteria-logical-${(index + 1)}`}
                          onChange={evt => {
                            // evt.stopPropagation();
                            this.listenFieldChange(index, 'logical', evt.target.value);
                          }}
                        >
                          <option value="and">AND</option>
                          <option value="or">OR</option>
                        </select>
                      </div>
                      <div className={styles.field}>
                        <select
                          onChange={evt => {
                            // evt.stopPropagation();
                            this.listenFieldChange(index, 'field', evt.target.value);
                          }}
                          id={`criteria-field-${(index + 1)}`}
                        >
                          {columns && columns.map(obj => (
                              <option value={`${obj.column_name}`} key={obj.label}>{obj.label}</option>
                            ))
                          }
                        </select>
                      </div>
                      <div className={styles.operator}>
                        <select
                          id={`criteria-operator-${(index + 1)}`}
                          onChange={evt => {
                            // evt.stopPropagation();
                            this.listenFieldChange(index, 'operator', evt.target.value);
                          }}
                        >
                          {comparators && comparators[condition.column_type] &&
                            comparators[condition.column_type].map(dataType => (
                              <option key={dataType}>{dataType}</option>
                            ))
                          }
                        </select>
                      </div>
                      <div className={styles.value}>
                        {!condition.isSelectType &&
                          <input
                            type="text"
                            onBlur={evt => {
                              this.listenFieldChange(index, 'input', evt.target.value);
                            }}
                            id={`criteria-input-${(index + 1)}`}
                          />
                        }
                        {condition.isSelectType &&
                          <select
                            id={`criteria-additional-${(index + 1)}`}
                            onChange={evt => {
                              // evt.stopPropagation();
                              this.listenFieldChange(index, 'additional', evt.target.value);
                            }}
                          >
                            {condition && condition.additional &&
                              condition.additional.map(iter => (
                                <option key={iter.id}>{iter.name}</option>
                              ))
                            }
                          </select>
                        }
                      </div>
                      {(index > 0) &&
                        <span
                          role="presentation"
                          className={styles.remove}
                          onClick={() => { remove(index); }}
                        >
                          <i className="fa fa-minus" aria-hidden="true" />
                        </span>
                      }
                      {(index + 1) === criteria.length &&
                        <span
                          role="presentation"
                          className={styles.add}
                          onClick={this.addCriteria}
                        >
                          <i className="fa fa-plus" aria-hidden="true" />
                        </span>
                      }
                    </div>
                    ))
                  }
                </div>
                <div className={`${styles.order}`}>
                  <input type="text" placeholder="criteria order" id="searchorder" />
                </div>
                <div className={styles.apply}>
                  <button
                    onClick={this.props.params.applyCriteria}
                    className="btn btn-primary font-wt-600 font-14"
                  >
                    Apply
                  </button>
                </div>
              </div>
            </div>
          </section>
        </ModalBody>
      </Modal>
    );
  }
}

AdvancedSearch.propTypes = {
  params: PropTypes.oneOfType([
    PropTypes.object
  ])
};

AdvancedSearch.defaultProps = {
  params: null
};

export default AdvancedSearch;
