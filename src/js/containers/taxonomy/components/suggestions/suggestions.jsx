import React from 'react';
import PropTypes from 'prop-types';
// custom files
import styles from './style.scss';
import { noFunc, isEmpty } from '../../../../utils/common';

const Suggestions = ({ list, selectValue }) => (
  <ul className={`slim-scroll ${styles.list} ${list.length === 0 ? 'hide' : ''}`}>
    {list.map(r => (
      <li
        key={r.tag_association_id}
        role="presentation"
        onClick={() => { selectValue(r) }}
        className={styles.item}
      >
        <b>{r.name}</b>
        {!isEmpty(r.parent_tag_name) &&
          <React.Fragment>
            <br/>
            <span>{`Child tag of ${r.parent_tag_name}`}</span>
          </React.Fragment>
        }
      </li>
    ))}
  </ul>
);

Suggestions.propTypes = {
  selectValue: PropTypes.func,
  list: PropTypes.arrayOf(PropTypes.object)
};

Suggestions.defaultProps = {
  list: [],
  selectValue: noFunc
};

export default Suggestions;
