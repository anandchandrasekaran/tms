import { put } from 'redux-saga/effects';

import { request, formatMessage } from '../../utils/common';

export function* createTag({ data, projectId }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().post(`/v1/projects/${projectId}/tags/`, data);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'success',
        message: 'New tag created'
      }
    });
    yield put({ type: 'TAGS:FETCH', projectId });
    yield put({ type: 'TAG:CREATE:SUCCESS', data: response.result || {} });
  } else {
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in creating new tag'
      }
    });
  }
}

export function* createChildTag({ data, tagId }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().post(`/v1/tags/${tagId}/`, data);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'success',
        message: 'Child tag added'
      }
    });
    yield put({ type: 'CHILD_TAG:CREATE:SUCCESS', data: response || {} });
  } else {
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in adding new child tag'
      }
    });
  }
}

/**
 * Fetches the current user by session
 *
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* fetchTags({ projectId }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().get(`/v1/projects/${projectId}/tags/`);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({ type: 'TAGS:FETCH:SUCCESS', data: response.result || [] });
  } else {
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in getting taxonomy tags'
      }
    });
  }
}

/**
 * Fetch single user detail
 *
 * @param      {<type>}  action  The action
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* fetchTagData({ tagId }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().get(`/v1/tags/${tagId}/`);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({ type: 'TAG_DATA:FETCH:SUCCESS', data: response || {} });
  } else {
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in getting tag data info'
      }
    });
  }
}

export function* detachTag({ projectId, tagId }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().delete(`/v1/projects/${projectId}/tags/${tagId}/`);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'success',
        message: 'Tag deleted'
      }
    });
    yield put({ type: 'TAGS:FETCH', projectId });
    yield put({ type: 'TAG:DELETE:SUCCESS', data: response.result || {} });
  } else {
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in deleting tag'
      }
    });
  }
}
