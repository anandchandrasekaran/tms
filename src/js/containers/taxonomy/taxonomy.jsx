import React, { Component } from 'react';
import PropTypes from 'prop-types';
import registry from 'app-registry';
// custom components
import styles from './style.scss';
import Request from '../../services/request';
import Upload from '../../components/Upload';
import Suggestions from './components/suggestions';
import Breadcrumb from '../../components/Breadcrumb';
import ConfirmOption from './components/confirmOption';
import AdvancedSearch from './components/advancedSearch';
import { getParameterValuesFromHash, extractQueryParams } from '../../utils/routerHooks';
import { noFunc, isEmpty, formatMessage, isArray, convertToObject } from '../../utils/common';

let treeRoot = {};
let outerUpdate = null;
let outerCenterNode = null;
const polyKeys = [];
const searchFields = [{
  key: 'field',
  selector: 'Field'
}, {
  key: 'operator',
  selector: 'Operator'
}, {
  key: 'additional',
  selector: 'Additional'
}, {
  key: 'input',
  selector: 'Input'
}, {
  key: 'logical',
  selector: 'Logical'
}];
const searchFieldsObj = convertToObject('key', searchFields);

const Options = ({ tag, styles, hasAccess, checkToHighlight, selectedTag, addChildTag, activeTags, fetchChildTag }) => (
  <React.Fragment>
    {tag.children && tag.children.map(obj => (
      <li
        key={`${tag.tag_id}-${obj.tag_id}`}
        role="presentation"
        id={`tag-${obj.tag_id}`}
        onClick={evt => {
          evt.stopPropagation();
          fetchChildTag(obj);
        }}
      >
        <i
          aria-hidden="true"
          className={`fa fa-caret-${activeTags.indexOf(obj.tag_association_id) === -1 ? 'right' : 'down'}`}
        />
        <span
          style={{
            borderLeft: !tag.is_approved ? '3px solid #DE6E34' : ''
          }}
          className={`${styles.text} ${checkToHighlight(obj, selectedTag, activeTags) ? styles.hglt : ''}`}
        >
          &nbsp;&nbsp;{obj.name}&nbsp;&nbsp;
          {polyKeys.indexOf(obj.tag_id) !== -1 &&
            <i title="poly key" className="fa fa-product-hunt" aria-hidden="true" />
          }
        </span>
        <ul className={`${styles.sublist} ${activeTags.indexOf(obj.tag_association_id) === -1 ? 'hide' : 'show'}`}>
          <Options
            tag={obj}
            styles={styles}
            hasAccess={hasAccess}
            activeTags={activeTags}
            selectedTag={selectedTag}
            addChildTag={addChildTag}
            fetchChildTag={fetchChildTag}
            checkToHighlight={checkToHighlight}
          />
          {hasAccess &&
            <li
              role="presentation"
              onClick={evt => {
                evt.stopPropagation();
                addChildTag(obj);
              }}
            >
              <i className="fa fa-plus" aria-hidden="true" />
              <span className={styles.text}>&nbsp;&nbsp;Add</span>
            </li>
          }
        </ul>
      </li>
      )
    )}
  </React.Fragment>
);

class Taxonomy extends Component {
  constructor(props) {
    super(props);
    this.searchOrder = '';
    this.projectId = getParameterValuesFromHash('/project/:projectId/project').projectId;
    this.sideMenu = [{
      name: 'default',
      link: '',
      icon: ''
    }, {
      name: 'Projects',
      link: '/project',
      icon: 'home'
    }, {
      name: 'User mapping',
      link: `/project/${this.projectId}`,
      icon: 'user-plus'
    }, {
      name: 'Users',
      link: '/user',
      icon: 'users'
    }, {
      name: 'Notifications',
      link: '/notifications',
      icon: 'envelope'
    }];
    this.state = {
      isNew: true,
      formData: {},
      treeData: [],
      fileName: '',
      fileSize: '',
      querySet: {},
      criteria: [],
      parentTag: {},
      isAdmin: false,
      dataToDraw: {},
      isChild: false,
      activeTags: [],
      selectedTag: {},
      showGraph: false,
      showModal: false,
      searchResult: [],
      uploadPercent: 0,
      enableExport: true,
      exportPercent: '0',
      hasAccess: false,
      dynamicFields: [],
      criteriaOrder: '',
      userPermission: {},
      expandGraph: false,
      enableUpload: false,
      processedPercent: '0',
      showUploadModal: false,
      selectedTab: 'hierarchy',
      uploadStatus: {
        isFailed: false,
        isSuccess: false,
        isUploading: false,
        selectFields: false,
        isValidating: false,
        isUploadError: false
      }
    };
  }

  componentDidMount() {
    this.fetchTags();
    this.checkImportStatus();
    this.fetchUserPermission();
    this.fetchAdvanceSearchValues();
    this.props.fetchCurrentProject(this.projectId);
  }

  componentWillUnmount() {
    this.props.resetValues();
  }

  fetchAdvanceSearchValues = () => {
    const { querySet, criteria } = this.state;
    Request.get('/v1/search/details/').then(result => {
      const { response, isSuccess } = result;
      if (isSuccess) {
        const { columns, allowed_values, comparators } = response;
        if (columns && columns.length > 0) {
          columns.forEach((column, index) => {
            column.id = index + 1;
            column.selectedInput = '';
            column.selectedField = column.column_name;
            column.selectedOperator = comparators[column.column_type][0];
            column.isSelectType = !isEmpty(allowed_values[column.column_type]);
            if (column.isSelectType) {
              const values = [];
              allowed_values[column.column_type][column.column_name].forEach(value => {
                if (!isEmpty(value.name)) {
                  values.push(value);
                }
              });
              column.additional = values;
              allowed_values[column.column_type][column.column_name] = values;
            }
          });
        }
        this.columnsObj = convertToObject('column_name', columns);
        criteria.push(JSON.parse(JSON.stringify(columns[0])));
        this.setState({ querySet: response, criteria });
      } else {
        this.props.notify({
          type: 'error',
          message: formatMessage(response) || 'Error in fetching advance search details'
        });
      }
    });
  };

  setGraphData = dataToDraw => {
    this.setState({
      dataToDraw
    }, () => {
      this.initializeGraph();
      // this.highlightNode();
      this.locateAndRenderGraph();
    });
  };

  toggleView = () => {
    const { showGraph, dataToDraw } = this.state;
    this.setState({
      expandGraph: false,
      showGraph: !showGraph
    }, () => {
      if (this.state.showGraph) {
        this.toggleGraph();
        this.setGraphData(dataToDraw);
      }
    });
  };

  isParenthesisMatching = str => {
    const stack = [];
    const open = {
      '{': '}',
      '[': ']',
      '(': ')'
    };
    const closed = {
      '}': true,
      ']': true,
      ')': true
    };
    for (let index = 0; index < str.length; index += 1) {
      const char = str[index];
      if (open[char]) {
        stack.push(char);
      } else if (closed[char]) {
        if (open[stack.pop()] !== char) return false;
      }
    }
    return stack.length === 0;
  };

  getSearchOrder = () => {
    let searchOrder = ['('];
    this.state.criteria.forEach((criteria, index) => {
      searchOrder.push(`${criteria.selectedLogical ? ` ${criteria.selectedLogical} ` : ''}{${index}}`);
    });
    searchOrder.push(')');
    this.searchOrder = searchOrder.join('');
    return this.searchOrder;
  };

  showAdvancedSearch = () => {
    this.setState({
      showAdvancedSearch: true
    }, () => {
      this.updateDropdown();
    });
  };

  closeAdvancedSearch = () => {
    this.setState({ showAdvancedSearch: false });
  };

  addCriteria = () => {
    const { querySet, criteria } = this.state;
    const newCriteria = JSON.parse(JSON.stringify(querySet.columns[0]));
    newCriteria.selectedLogical = 'and';
    criteria.push(newCriteria);
    this.setState({ criteria }, () => {
      this.updateDropdown();
    });
  };

  updateDropdown = () => {
    const { criteria } = this.state;
    searchFields.forEach(field => {
      criteria.forEach((tuple, position) => {
        const domValue = tuple[`selected${field.selector}`];
        if (!isEmpty(domValue)) {
          document.getElementById(`criteria-${field.key}-${position + 1}`).value = domValue;
        }
      });
    });
    document.getElementById('searchorder').value = this.getSearchOrder();
  };

  listenChange = (index, selector, value) => {
    const { criteria, querySet } = this.state;
    const currentObj = criteria[index];
    currentObj[`selected${searchFieldsObj[selector].selector}`] = value;
    if (selector === 'field') {
      if (!isEmpty(querySet.allowed_values[(this.columnsObj[value]).column_type])) {
        currentObj.isSelectType = true;
        currentObj.additional = (querySet.allowed_values[(this.columnsObj[value]).column_type])[currentObj.selectedField];
        currentObj.selectedAdditional = currentObj.additional[0].name;
      } else {
        currentObj.isSelectType = false;
        currentObj.additional = [];
        currentObj.selectedAdditional = '';
      }
      currentObj.column_type = (this.columnsObj[value]).column_type;
      currentObj.selectedOperator = querySet.comparators[currentObj.column_type][0];
    }
    this.setState({ criteria }, () => {
      this.updateDropdown();
    });
  };

  applyCriteria = () => {
    let searchOrder = document.getElementById('searchorder').value;
    searchOrder = searchOrder.trim();
    if (isEmpty(searchOrder)) {
      searchOrder = this.getSearchOrder();
    }
    // validation of search order
    if (!this.isParenthesisMatching(searchOrder)) {
      this.props.notify({
        type: 'error',
        message: 'Erroneous search order. Please correct it'
      });
      return;
    }
    const requestPayload = {
      limit: 10,
      criterias: [],
      search_order: ''
    };
    this.state.criteria.forEach((criteria, index) => {
      requestPayload.criterias.push({
        column_name: criteria.selectedField,
        comparator: criteria.selectedOperator,
        value: criteria.isSelectType ? criteria.selectedAdditional : criteria.selectedInput
      });
    });
    requestPayload.search_order = searchOrder;
    console.log('-requestPayload-', requestPayload);
    // Fetching tags of the selected project
    this.props.toggleLoader(true);
    Request.post(`/v1/projects/${this.projectId}/tags/search/advanced/`, requestPayload).then(result => {
      this.props.toggleLoader(false);
      const { response, isSuccess } = result;
      if (isSuccess) {
        this.setState({ searchResult: response.result || [], showAdvancedSearch: false });
      } else {
        this.props.notify({
          type: 'error',
          message: formatMessage(response) || 'Error in advanced search'
        });
      }
    });
  };

  deleteField = index => {
    const { criteria } = this.state;
    criteria.splice(index, 1);
    this.setState({ criteria }, () => {
      this.updateDropdown();
    });
  };

  fetchTags = showLoader => {
    // Fetching tags of the selected project
    if (showLoader) {
      this.props.toggleLoader(true);
    }
    Request.get(`/v1/projects/${this.projectId}/tags/`).then(result => {
      if (showLoader) {
        this.props.toggleLoader(false);
      }
      const { response, isSuccess } = result;
      if (isSuccess) {
        this.setState({
          treeData: response.result || []
        }, () => {
          const { dataToDraw } = this.state;
          if (!isEmpty(dataToDraw) && !isEmpty(dataToDraw.tag_id)) {
            this.fetchTagTree(dataToDraw);
          }
        });
      } else {
        this.props.notify({
          type: 'error',
          message: formatMessage(response) || 'Error in fetching tags'
        });
      }
    });
  };

  fetchTagTree = (tag, isFromSearch) => {
    let associationId = null;
    const { treeData } = this.state;
    if (isFromSearch) {
      associationId = isEmpty(tag.hierarchy) ? tag.tag_association_id : (tag.hierarchy.split('>'))['1'];
    } else {
      associationId = tag.tag_association_id;
    }
    associationId = parseInt(associationId, 10);
    // Fetching tag hierarchy of the selected tag
    this.props.toggleLoader(true);
    const requestUrl = `/v1/projects/${this.projectId}/tags/tree/?parent_tag_association_id=${associationId}`;
    Request.get(requestUrl).then(result => {
      this.props.toggleLoader(false);
      const { response, isSuccess } = result;
      if (isSuccess) {
        const all = [];
        const hierarchies = response.result || [];
        // Finding poly-keys
        hierarchies.forEach(iteratee => {
          const loop = obj => {
            if (obj.children && obj.children.length > 0) {
              obj.children.forEach(child => {
                loop(child);
              });
            } else if (!isEmpty(obj.tag_id)) {
              const tagId = parseInt(obj.tag_id, 10);
              if (all.indexOf(tagId) === -1) {
                all.push(tagId);
              } else if (polyKeys.indexOf(tagId) === -1) {
                polyKeys.push(tagId);
              }
            }
          };
          loop(iteratee);
        });
        const children = hierarchies[0] ? (hierarchies[0].children || []) : [];
        if (isFromSearch) {
          treeData.forEach(data => {
            if (data.tag_association_id === associationId) {
              data.children = children;
            }
          });
        } else {
          treeData.forEach(data => {
            if (data.tag_id === tag.tag_id) {
              data.children = children;
            }
          });
        }
        this.setState({ treeData }, () => {
          if (this.state.treeData && this.state.treeData.length > 0) {
            if (isFromSearch) {
              this.state.treeData.forEach(data => {
                if (data.tag_association_id === associationId) {
                  this.setGraphData(data);
                }
              });
            } else {
              this.state.treeData.forEach(obj => {
                if (!isEmpty(obj.tag_id) && tag.tag_id === obj.tag_id) {
                  this.setGraphData(obj);
                }
              });
            }
          }
        });
      } else {
        this.props.notify({
          type: 'error',
          message: formatMessage(response) || 'Error in fetching tags hierarchy'
        });
      }
    });
  };

  fetchUserPermission = () => {
    this.props.toggleLoader(true);
    Request.get(`/v1/projects/${this.projectId}/user/`).then(result => {
      this.props.toggleLoader(false);
      const { response, isSuccess } = result;
      if (isSuccess) {
        const { permission } = response;
        this.setState({
          userPermission: response || {},
          isAdmin: permission && permission === 'ADMIN',
          hasAccess: permission && permission !== 'READ'
        });
      } else {
        this.props.notify({
          type: 'error',
          message: formatMessage(response) || 'Error in fetching user permission'
        });
      }
    });
  };

  getRequestPayload = (name, description) => {
    const additionalFields = [];
    const { dynamicFields } = this.state;
    if (dynamicFields && dynamicFields.length > 0) {
      dynamicFields.forEach(field => {
        if (field.isManuallyAdded) {
          const { attribute_name, attribute_value } = field;
          additionalFields.push({
            attribute_name,
            attribute_value
          });
        }
      });
    }
    const requestPayload = { name, description };
    if (additionalFields.length > 0) {
      requestPayload.dynamic_fields = additionalFields;
    }
    return requestPayload;
  };

  createTag = (name, description) => {
    const requestPayload = this.getRequestPayload(name, description);
    // First level of tag creation
    this.props.toggleLoader(true);
    Request.post(`/v1/projects/${this.projectId}/tags/`, requestPayload).then(result => {
      this.props.toggleLoader(false);
      const { response, isSuccess } = result;
      if (isSuccess) {
        this.props.notify({
          type: 'success',
          message: 'New tag created'
        });
        this.resetFields();
        this.fetchTags();
      } else {
        this.props.notify({
          type: 'error',
          message: formatMessage(response) || 'Error in creating new tag'
        });
      }
    });
  };

  updateTag = (name, description, selectedTag) => {
    let ids = [];
    const additionalFields = [];
    const { formData, dynamicFields } = this.state;
    const { dynamic_fields } = formData;

    if (dynamicFields && dynamicFields.length > 0) {
      dynamic_fields.forEach(field => {
        ids.push(field.id);
      });

      dynamicFields.forEach(field => {
        const { id, attribute_name, attribute_value, isManuallyAdded } = field;
        const obj = { attribute_name, attribute_value };
        if (!isManuallyAdded) {
          obj.id = id;
          const index = ids.indexOf(id);
          if (index !== -1) {
            // ids.splice(index, 1);
            ids = ids.filter(iter => (id !== iter));
          }
        }
        additionalFields.push(obj);
      });
    }
    const requestPayload = { name, description };
    if (additionalFields.length > 0) {
      requestPayload.dynamic_fields = additionalFields;
    }
    if (ids && ids.length > 0) {
      requestPayload.deleted_dynamic_fields = ids;
    }
    // Tag update
    this.props.toggleLoader(true);
    Request.patch(`/v1/projects/${this.projectId}/tags/${selectedTag.tag_id}/`, requestPayload).then(result => {
      this.props.toggleLoader(false);
      const { response, isSuccess } = result;
      if (isSuccess) {
        this.props.notify({
          type: 'success',
          message: 'Tag updated'
        });
        this.resetFields();
        this.fetchTags();
        this.setState({ isNew: true });
      } else {
        this.props.notify({
          type: 'error',
          message: formatMessage(response) || 'Error in updating tag details'
        });
      }
    });
  };

  createChildTag = (name, description, selectedTag) => {
    const requestPayload = this.getRequestPayload(name, description);
    requestPayload.parent_tag_association_id = selectedTag.tag_association_id;
    this.props.toggleLoader(true);
    Request.post(`/v1/projects/${this.projectId}/tags/`, requestPayload).then(result => {
      this.props.toggleLoader(false);
      const { response, isSuccess } = result;
      if (isSuccess) {
        this.props.notify({
          type: 'success',
          message: 'New child tag created'
        });
        this.resetFields();
        this.fetchTags();
      } else {
        this.props.notify({
          type: 'error',
          message: formatMessage(response) || 'Error in creating new child tag'
        });
      }
    });
  };

  locateAndRenderGraph = () => {
    this.clearAll(treeRoot);
    this.expandAll(treeRoot);
    // outerUpdate(treeRoot);
    this.searchTree(treeRoot, true);
    if (treeRoot.children) {
      treeRoot.children.forEach(this.collapseAllNotFound);
    }
    outerUpdate(treeRoot);
    if (treeRoot.children) {
      treeRoot.children.forEach(this.centerSearchTarget);
    }
    this.resetTimer = window.setTimeout(() => {
      window.clearTimeout(this.resetTimer);
      this.resetNodes();
      this.highlightNode();
    }, 800);
  };

  goBack = () => {
    this.context.router.history.push(`/project/${this.projectId}`);
  };

  fetchTag = (tag, isFromSearch) => {
    const { isNew, selectedTag, activeTags } = this.state;
    if (isNew || selectedTag.tag_id !== tag.tag_id) {
      this.props.toggleLoader(true);
      Request.get(`/v1/projects/${this.projectId}/tags/${tag.tag_id}/`).then(result => {
        this.props.toggleLoader(false);
        const { response, isSuccess } = result;
        if (isSuccess) {
          let hierarchies = isArray(response.hierarchies) ? response.hierarchies : [response.hierarchies];
          if (hierarchies.length > 0) {
            hierarchies.forEach(hierarchy => {
              if (!isEmpty(hierarchy)) {
                const temp = hierarchy.split('>');
                const total = temp.length;
                if (temp.length > 0) {
                  temp.forEach(id => {
                    if (!isEmpty(id)) {
                      const value = parseInt(id, 10);
                      if (activeTags.indexOf(value) === -1) {
                        activeTags.push(value);
                      }
                    }
                  });
                }
              }
            });
          }
          // this.eventFire(document.getElementById(`node-${tag.tag_id}`), 'click');
          this.setState({
            activeTags,
            isNew: false,
            parentTag: {},
            selectedTag: tag,
            formData: response || {},
            dynamicFields: response.dynamic_fields ? JSON.parse(JSON.stringify(response.dynamic_fields)) : []
          }, () => {
            if (this.state.showGraph) {
              this.locateAndRenderGraph();
            }
          });
        }
      });
    }
  };

  getTag = tag => {
    this.updateActiveTags(tag.tag_association_id);
    this.fetchTag(tag);
  };

  resetFields = () => {
    this.setState({ formData: {}, dynamicFields: [] });
  };

  addNewTag = () => {
    this.setState({
      isNew: true,
      parentTag: {},
      isChild: false,
      selectedTag: {},
      showGraph: false,
      expandGraph: false
    }, () => {
      this.resetFields();
    });
  };

  createOrUpdateTag = () => {
    const { isChild, selectedTag, formData, isNew } = this.state;
    const { name, description } = formData;
    if (isEmpty(name)) {
      this.props.notify({ type: 'error', message: 'Name is mandatory' });
    } else {
      if (isNew) {
        if (isChild) {
          // First level of tag creation
          this.createChildTag(name, description, selectedTag);
        } else {
          this.createTag(name, description);
        }
      } else {
        this.updateTag(name, description, selectedTag);
      }
    }
  };

  deleteTag = () => {
    const { selectedTag } = this.state;
    this.props.toggleLoader(true);
    Request.delete(
      `/v1/projects/${this.projectId}/tags/${selectedTag.tag_id}/`
    ).then(result => {
      this.updateActiveTags(selectedTag.tag_association_id);
      this.props.toggleLoader(false);
      const { response, isSuccess } = result;
      if (isSuccess) {
        this.props.notify({
          type: 'success',
          message: response.message
        });
        this.resetFields();
        this.fetchTags();
        this.setState({ isNew: true });
      } else {
        this.props.notify({
          type: 'error',
          message: formatMessage(response) || 'Error in deleting tag'
        });
      }
    });
  };

  resetOrDeleteTag = () => {
    const { isNew } = this.state;
    if (isNew) {
      this.resetFields();
    } else {
      this.deleteTag();
    }
  };

  /**
   * handleHide
   *
   * @return     {<type>}  Manages to hide and reset the state values on modal close
   */
  handleHide = () => {
    this.setState({ showModal: false });
  };

  proceedToDisAssociate = merge => {
    this.setState({ showModal: false });
    const { selectedTag, dataToDraw } = this.state;
    const associationId = selectedTag.tag_association_id;
    this.props.toggleLoader(true);
    Request.delete(`/v1/tags/${associationId}/?to_parent=${merge}`).then(result => {
      this.props.toggleLoader(false);
      this.updateActiveTags(associationId);
      const { response, isSuccess } = result;
      if (isSuccess) {
        this.props.notify({
          type: 'success',
          message: response.message
        });
        this.resetFields();
        this.fetchTags();
        this.setState({
          isNew: true,
          selectedTag: {},
          dataToDraw: selectedTag.tag_id === dataToDraw.tag_id ? {} : dataToDraw
        });
      } else {
        this.props.notify({
          type: 'error',
          message: formatMessage(response) || 'Error in disassociating tag'
        });
      }
    });
  };

  disAssociate = () => {
    const { selectedTag } = this.state;
    const { parent_tag_id, children } = selectedTag;
    if (!isEmpty(parent_tag_id) && parent_tag_id !== 'null' && children && children.length > 0) {
      this.setState({ showModal: true });
    } else {
      this.proceedToDisAssociate(false);
    }
  };

  updateActiveTags = tagId => {
    // Toggling the open and close of the tags
    const { activeTags } = this.state;
    const tagIndex = activeTags.indexOf(tagId);
    if (tagIndex !== -1) {
      activeTags.splice(tagIndex, 1);
    } else {
      activeTags.push(tagId);
    }
    this.setState({ activeTags });
  };

  approveTag = () => {
    const { selectedTag } = this.state;
    this.props.toggleLoader(true);
    Request.post(`/v1/projects/${this.projectId}/tag_approval/`, {
      tag_ids: [selectedTag.tag_id]
    }).then(result => {
      this.props.toggleLoader(false);
      const { response, isSuccess } = result;
      if (isSuccess) {
        this.props.notify({
          type: 'success',
          message: response.message
        });
        this.resetFields();
        this.fetchTags();
        this.setState({ isNew: true });
      } else {
        this.props.notify({
          type: 'error',
          message: formatMessage(response) || 'Error in approving tag'
        });
      }
    });
  };

  refreshTag = () => {
    console.log('Refresh tag detail');
  };

  addChildTag = tag => {
    this.setState({
      isNew: true,
      isChild: true,
      parentTag: tag,
      selectedTag: tag,
      showGraph: false,
      expandGraph: false
    }, () => {
      this.resetFields();
    });
  };

  getValueFromElement = evt => {
    const { formData } = this.state;
    const { id, value } = evt.target;
    return { field: id, value, formData };
  };

  trimFieldValue = evt => {
    const { field, value, formData } = this.getValueFromElement(evt);
    formData[field] = isEmpty(value) ? '' : value.trim();
    this.setState({ formData });
  };

  trimDynamicFieldValue = evt => {
    const { dynamicFields } = this.state;
    const { field, value } = this.getValueFromElement(evt);
    let key = 'attribute_name';
    const meta = field.split('-');
    if (meta[1] === 'value') {
      key = 'attribute_value';
    }
    dynamicFields.forEach(obj => {
      if (obj.id === parseInt(meta[0], 10)) {
        obj[key] = isEmpty(value) ? '' : value.trim();
      }
    });
    this.setState({ dynamicFields });
  };

  onChangeFormFields = evt => {
    const { field, value, formData } = this.getValueFromElement(evt);
    formData[field] = value;
    this.setState({ formData });
  };

  onChangeDynamicFields = evt => {
    const { dynamicFields } = this.state;
    const { field, value } = this.getValueFromElement(evt);
    let key = 'attribute_name';
    const meta = field.split('-');
    if (meta[1] === 'value') {
      key = 'attribute_value';
    }
    dynamicFields.forEach(obj => {
      if (obj.id === parseInt(meta[0], 10)) {
        obj[key] = value;
      }
    });
    this.setState({ dynamicFields });
  };

  checkToHighlight = (tag, selectedTag, activeTags) => (
    ((tag.tag_id === selectedTag.tag_id && activeTags.indexOf(tag.tag_id) !== -1) || tag.tag_id === selectedTag.tag_id)
  );

  /**
   * { function_description }
   */
  adjustHeight = () => {
    const timer = window.setTimeout(() => {
      window.clearTimeout(timer);
      const contentHeight = document.getElementById('content').offsetHeight;
      const node = d3.select('#graph').select('svg').node().getBBox();
      const imgWidth = Math.ceil(node.width);
      const imgHeight = Math.ceil(node.height);
      const graph = document.getElementById('graph');
      const summary = document.getElementById('summary').offsetWidth;
      const sideMenu = document.getElementById('sidemenu').offsetWidth;
      const parentElWidth = graph.parentElement.clientWidth;
      graph.style.width = `${Math.ceil(parentElWidth > imgWidth ? parentElWidth : imgWidth + 25)}px`;
      const pageHeight = imgHeight < contentHeight ? contentHeight : (imgHeight + 20);
      graph.style.height = `${pageHeight}px`;
    }, 500);
  };

  highlightNode = () => {
    const { selectedTag } = this.state;
    if (!isEmpty(selectedTag.tag_id)) {
      d3.selectAll(`#node-${selectedTag.tag_id}`).transition().duration(750).style('stroke', 'red');
      const elmntToView = $(`#tag-${selectedTag.tag_id}`);
      elmntToView[0].scrollIntoView({ behavior: 'smooth' });
    }
  };

  resetNodes = () => {
    if (this.state.showGraph) {
      d3.selectAll('.node circle').transition().duration(200).style('stroke', 'steelblue');
    }
  };

  eventFire = (el, etype) => {
    if (el.fireEvent) {
      el.fireEvent('on' + etype);
    } else {
      const evObj = document.createEvent('Events');
      evObj.initEvent(etype, true, false);
      el.dispatchEvent(evObj);
    }
  };

  clearAll = d => {
    d.class = '';
    if (d.children) {
      d.children.forEach(this.clearAll);
    } else if (d._children) {
      d._children.forEach(this.clearAll);
    }
  };

  expandAll = d => {
    if (d._children) {
      d.children = d._children;
      d.children.forEach(this.expandAll);
      d._children = null;
    } else if (d.children) {
      d.children.forEach(this.expandAll);
    }
  };

  collapseAllNotFound = d => {
    if (d.children) {
      if (d.class !== 'found') {
        d._children = d.children;
        d._children.forEach(this.collapseAllNotFound);
        d.children = null;
      } else {
        d.children.forEach(this.collapseAllNotFound);
      }
    }
  };

  centerSearchTarget = d => {
    if (d.search_target) {
      outerCenterNode(d);
      console.log(`Found search target: ${d.name}`);
    }
    if (d.children) {
      d.children.forEach(this.centerSearchTarget);
    }
  };

  searchTree = (d, first_call = false) => {
    if (d.children) {
      d.children.forEach(this.searchTree);
    } else if (d._children) {
      d._children.forEach(this.searchTree);
    }
    const searchFieldValue = eval('d.name');
    if (!isEmpty(searchFieldValue) && searchFieldValue === this.state.selectedTag.name) {
      if (first_call) {
        d.search_target = true;
      } else {
        d.search_target = false;
      }
      // Walk parent chain
      let parent = d;
      const ancestors = [];
      while (typeof(parent) !== 'undefined') {
        ancestors.push(parent);
        parent.class = 'found';
        parent = parent.parent;
      }
    }
  };

  initializeGraph = () => {
    // ************** Generate the tree diagram  *****************
    const margin = { top: 40, right: 120, bottom: 20, left: 120 };
    const width = 960 - margin.right - margin.left;
    const height = 500 - margin.top - margin.bottom;
    const { dataToDraw, selectedTag } = this.state;

    let index = 0;
    const duration = 750;
    const root = JSON.parse(JSON.stringify(dataToDraw));
    root.x0 = height / 2;
    root.y0 = 0;
    treeRoot = root;

    const tree = d3.layout.tree().nodeSize([ 95 , 0 ]).separation((a, b) => (a.parent === b.parent ? 1 : 1.25));

    const diagonal = d3.svg.diagonal().projection(d => ([d.x, d.y]));
    // Removing existing DOM content
    d3.select('#graph').selectAll('*').remove();

    // Define the zoom function for the zoomable tree
    function zoom() {
      d3.select('g').attr('transform', `translate(${d3.event.translate})scale(${d3.event.scale})`);
    }

    // define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
    const zoomListener = d3.behavior.zoom().scaleExtent([0.1, 4]).on('zoom', zoom);

    const svg = d3.select('#graph').append('svg')
      .attr('width', '100%')
      .attr('height', '100%')
      .attr('overflow', 'auto')
      .call(zoomListener)
      .append('g')
      .attr('transform', `translate(${margin.left + 150}, ${margin.top})`);

    d3.select(self.frameElement).style('height', '500px');

    const centerNode = source => {
      const scale = zoomListener.scale();
      let x = -source.x0;
      let y = -source.y0;
      x = x * scale + ($('#graph').width()) / 2;
      y = y * scale + ($('#graph').height()) / 2;
      d3.select('g').transition()
        .duration(duration)
        .attr('transform', `translate(${x}, ${y}) scale(${scale})`);
      zoomListener.scale(scale);
      zoomListener.translate([x, y]);
    };

    outerCenterNode = centerNode;

    function collapseLevel(d) {
      if (d.children) {
        if (d.depth > 0) {
          d._children = d.children;
          d._children.forEach(collapseLevel);
          d.children = null;
        } else {
          d.children.forEach(collapseLevel);
        }
      }
    }

    // Toggle children function
    function toggleChildren(d) {
      if (d.children) {
        d._children = d.children;
        d.children = null;
      } else if (d._children) {
        d.children = d._children;
        d._children = null;
      }
      return d;
    }

    // Compute the new tree layout.
    const _nodes = tree.nodes(root).reverse();
    _nodes.forEach(d => { d.y = d.depth * 120; });

    if (root && root.children) {
      root.children.forEach(collapseLevel);
    }

    // Toggle children on click.
    /**
     * { function_description }
     *
     * @param      {<type>}  d       { parameter_description }
     */
    const click = d => {
      this.updateActiveTags(this.state.selectedTag.tag_association_id);
      this.getTag(d);
      // // this.getTag(JSON.parse(JSON.stringify(d)));
      // if (d.children) {
      //   d._children = d.children;
      //   d.children = null;
      // } else {
      //   d.children = d._children;
      //   d._children = null;
      // }
      if (d3.event.defaultPrevented) return; // click suppressed
      d = toggleChildren(d);
      update(d);
      centerNode(d);
    };

    const dotme = text => {
      text.each(function() {
        var text = d3.select(this);
        const label = text.text();
        const wordLength = label.length;
        if (wordLength > 15) {
          text.text('').append('tspan').attr('class', 'elip').text(`${label.slice(0, 13)}...`);
        }
        // const words = text.text().split(/\s+/);
        // if (words.length > 1 && wordLength > 15) {
        //   const ellipsis = text.text('').append('tspan').attr('class', 'elip').text('...');
        //   const width = parseFloat(Math.ceil(text.attr('width'))) - Math.ceil(ellipsis.node().getComputedTextLength());
        //   const numWords = words.length;
        //   const tspan = text.insert('tspan', ':first-child').text(words.join(' '));
        //   // Try the whole line
        //   // While it's too long, and we have words left, keep removing words
        //   while (Math.ceil(tspan.node().getComputedTextLength()) > width && !isEmpty(words.length)) {
        //     words.pop();
        //     tspan.text(words.join(' '));
        //   }
        //   if (words.length === numWords) {
        //     ellipsis.remove();
        //   }
        // } else if (words.length === 1 && wordLength > 12) {
        //   text.text('').append('tspan').attr('class', 'elip').text(`${label.slice(0, 12)}...`);
        // }
      });
    };

    // Define the div for the tooltip
    var div = d3.select('body').append('div')
      .attr('class', 'tooltip')
      .style('opacity', 0);

    /**
     * Updates the given source.
     *
     * @param      {<type>}  source  The source
     */
    const update = source => {
      // Compute the new tree layout.
      const nodes = tree.nodes(root).reverse();
      const links = tree.links(nodes);

      // Normalize for fixed-depth.
      nodes.forEach(d => { d.y = d.depth * 120; });

      // Declare the nodes…
      const node = svg.selectAll('g.node').data(nodes, d => (d.id || (d.id = ++index)));

      // Enter the nodes.
      const nodeEnter = node.enter().append('g')
        .attr('class', 'node')
        .attr('transform', d => (`translate(${d.x}, ${d.y})`))
        .on('click', click);

      nodeEnter.append('circle')
        .attr('r', 10)
        .attr('id', d => (`node-${d.tag_id}`))
        .style('fill', d => (d._children ? 'lightsteelblue' : '#fff'));

      nodeEnter.append('text')
        .attr('y', d => (d.children || d._children ? ((18 + 4) * -1) : (18 + 4)))
        .attr('dy', '.35em')
        .attr('width', 95)
        .attr('class', 'dotme')
        .attr('text-anchor', 'middle')
        .text(d => (d.name))
        .style('fill-opacity', 1)
        .on('mouseover', d => {
          div.transition()
            .duration(200)
            .style('opacity', .9);
          div.html(d.name)
            .style('width', '100px')
            .style('display', `${d.name.length > 15 ? '' : 'none'}`)
            .style('height', 'auto')
            .style('overflow-wrap', 'break-word')
            .style('left', `${(d3.event.pageX)}px`)
            .style('top', `${(d3.event.pageY - 28)}px`);
          })
        .on('mouseout', d => {
          div.transition()
            .duration(500)
            .style('opacity', 0);
        });

      d3.selectAll('.dotme').call(dotme);

      // Transition nodes to their new position.
      const nodeUpdate = node.transition()
        .duration(duration)
        .attr('transform', d => (`translate(${d.x}, ${d.y})`));

      nodeUpdate.select('circle')
        .attr('r', 10)
        .style('fill', d => (d._children ? 'lightsteelblue' : '#fff'));

      nodeUpdate.select('text')
        .attr('text-anchor', d => (d.children ? 'end' : 'start'))
        .attr('y', d => (d.children ? '-25' : '25'))
        .attr('x', d => (d.children ? '25' : '-25'))
        .style('fill-opacity', 1);

      // Transition exiting nodes to the parent's new position.
      const nodeExit = node.exit().transition()
        .duration(duration)
        .attr('transform', d => (`translate(${source.x}, ${source.y})`))
        .remove();

      nodeExit.select('circle').style('display', 'none').attr('r', 10);

      nodeExit.select('text').style('display', 'none').style('fill-opacity', 1);

      // Declare the links…
      const link = svg.selectAll('path.link').data(links, d => (d.target.id));

      // Enter any new links at the parent's previous position.
      link.enter().insert('path', 'g')
        .attr('class', 'link')
        // .style('stroke', '#251a3e')
        .attr('d', d => {
          const o = { x: source.x0, y: source.y0 };
          return diagonal({ source: o, target: o });
        });

      // Transition links to their new position.
      link.transition()
        .duration(duration)
        .attr('d', diagonal)
        .style('stroke', d => {
        if (d.target.class === 'found') {
          return '#2E8B57'; // seagreen
        }
      });;

      // Transition exiting nodes to the parent's new position.
      link.exit().transition()
        .duration(duration)
        .style('display', 'none')
        .attr('d', d => {
          const o = { x: source.x, y: source.y };
          return diagonal({ source: o, target: o });
        })
        .remove();

      // Stash the old positions for transition.
      nodes.forEach(d => {
        d.x0 = d.x;
        d.y0 = d.y;
        // Adjust content div height based on the svg height
      });
      // this.adjustHeight();
    };

    outerUpdate = update;

    update(root);
  };

  disable = (isAdmin, isApproved, isNew) => {
    let disable = '';
    if (isNew) {
      disable = true;
    } else {
      if (isAdmin && !isApproved) {
        disable = false;
      } else {
        disable = true;
      }
    }
    return disable;
  };

  /**
   * handleUploadHide
   *
   * @return     {<type>}  Manages to hide and reset the state values on modal close
   */
  handleUploadHide = () => {
    const { uploadStatus } = this.state;
    if (!uploadStatus.isUploading || uploadStatus.isFailed || uploadStatus.isUploadError) {
      uploadStatus.isFailed = false;
      uploadStatus.isSuccess = false;
      uploadStatus.isUploading = false;
      uploadStatus.isValidating = false;
      uploadStatus.isUploadError = false;
      this.setState({
        uploadStatus,
        fileName: '',
        fileSize: '',
        uploadPercent: 0,
        showUploadModal: false
      }, () => {
        // this.props.resetPopupStatus();
        this.isUserCancelledUpload = false;
      });
    }
  };

  /**
   * resetUpload
   *
   * @return     {<type>}  Resets the upload related state values
   */
  resetUpload = () => {
    const { uploadStatus } = this.state;
    uploadStatus.isFailed = false;
    uploadStatus.isSuccess = false;
    uploadStatus.isUploading = false;
    uploadStatus.isValidating = false;
    uploadStatus.isUploadError = false;
    // Reset input field
    document.getElementById('uploadfile').val('');
    this.setState({
      uploadStatus,
      fileName: '',
      fileSize: '',
      showUploadModal: true,
      uploadPercent: 0
    }, () => {
      this.isUserCancelledUpload = false;
    });
  };

  /**
   * cancelUpload
   *
   * @return     {<type>}  To cancel the active upload by user
   */
  cancelUpload = () => {
    const { uploadPercent } = this.state;
    if (uploadPercent < 100 && this.xhr) {
      this.xhr.abort();
      this.isUserCancelledUpload = true;
    } else {
      this.props.notify({
        type: 'error',
        message: 'File upload already completed'
      });
    }
  };

  download = url => {
    this.props.toggleLoader(true);
    const link = document.createElement('a');
    // Set href as a local object URL
    link.setAttribute('href', url);
    // Set name of download
    link.setAttribute('download', (url.split('/').pop()).split('?')[0]);
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    this.props.toggleLoader(false);
  };

  checkImportStatus = () => {
    const clear = () => {
      if (this.statusTimer) {
        window.clearTimeout(this.statusTimer);
      }
    };
    clear();
    Request.get(`/v1/projects/${this.projectId}/tags/import/status/`).then(result => {
      const { response, isSuccess } = result;
      if (isSuccess) {
        // Ready, Success, Failure, Process
        if (response.import_status.toLowerCase() === 'process') {
          const { completed_rows, total_rows } = response;
          if (!isEmpty(completed_rows) && !isEmpty(total_rows)) {
            this.setState({ processedPercent: Math.ceil((completed_rows / total_rows) * 100) });
          }
          this.statusTimer = window.setTimeout(() => {
            clear();
            this.checkImportStatus();
          }, 10000);
        } else {
          this.setState({ enableUpload: true, processedPercent: '0' }, () => {
            this.fetchTags(false);
            clear();
          });
        }
      } else {
        clear();
        console.warn(formatMessage(response) || 'Error in checking the import status');
      }
    });
  };

  checkExportStatus = () => {
    const clear = () => {
      if (this.exportStatusTimer) {
        window.clearTimeout(this.exportStatusTimer);
      }
    };
    clear();
    Request.get(`/v1/projects/${this.projectId}/tags/export/status/`).then(result => {
      const { response, isSuccess } = result;
      if (isSuccess) {
        const status = response.import_status.toLowerCase();
        // Ready, Success, Failure, Process
        if (status === 'process') {
          const { completed_rows, total_rows } = response;
          if (!isEmpty(completed_rows) && !isEmpty(total_rows)) {
            this.setState({ exportPercent: Math.ceil((completed_rows / total_rows) * 100) });
          }
          this.exportStatusTimer = window.setTimeout(() => {
            clear();
            this.checkExportStatus();
          }, 8000);
        } else {
          this.props.notify({
            type: status === 'success' ? 'success' : 'error',
            message: `Export status: ${response.import_status}`
          });
          this.setState({ enableExport: true, exportPercent: '0' }, () => {
            if (!isEmpty(response.download_link)) {
              this.download(response.download_link);
            }
            clear();
          });
        }
      } else {
        clear();
        console.warn(formatMessage(response) || 'Error in checking the export status');
      }
    });
  };


  /**
   * Uploads a file.
   *
   * @return     {<type>}  Uploads the file and manages the upload related actions
   */
  uploadFile = () => {
    const { enableUpload } = this.state;
    if (!enableUpload) {
      return;
    }
    // Based on the upload type, access the file to upload
    const { files } = document.getElementById('uploadfile');
    const file = files[0];
    if (file) {
      const fileSize = file.size;
      const fileName = file.name;
      const fileType = file.type;
      console.log('Uploading file type: ', fileType);
      // TODO:: Detection of uploading file mime type needs to be added. Until then, disabling the file validation.
      const validFileTypes = ['text/csv', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'text/turtle'];
      // if (validFileTypes.indexOf(fileType) === -1) {
      if (!validFileTypes) {
        this.props.notify({
          type: 'error',
          message: 'Invalid file format. Please upload .csv or .ttl file'
        });
      } else {
        this.setState({ enableUpload: false });
        const { uploadStatus, isAdmin } = this.state;
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = () => {
          const formData = new FormData();
          formData.append('file', file);
          const config = registry.get('config');
          const xhr = new XMLHttpRequest();
          xhr.open('POST', `${config.baseApiUrl}/v1/projects/${this.projectId}/tags/import/`, true);
          // xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
          // xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
          xhr.setRequestHeader('Authorization', `Bearer ${registry.get('storage').getItem('authToken')}`);
          xhr.onload = () => {
            const response = JSON.parse(xhr.responseText);
            this.checkImportStatus();
            if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 201)) {
              uploadStatus.isFailed = false;
              uploadStatus.isSuccess = true;
              uploadStatus.isUploading = false;
              uploadStatus.isValidating = false;
              uploadStatus.isUploadError = false;
              this.setState({
                uploadStatus,
                successMsg: response.message
              }, () => {
                const timer = setTimeout(() => {
                  clearTimeout(timer);
                  this.fetchTags();
                  this.handleUploadHide();
                  const { user } = this.props;
                  if (!isAdmin) {
                    this.props.refreshNotifications();
                  }
                }, 2000);
              });
            } else {
              // resolve({ body: response, type: FAILED });
              console.error('Error: ', response);
              this.props.notify({
                type: 'error',
                message: formatMessage(response)
              });
            }
          };
          xhr.onerror = err => {
            console.error('Error in taxonomy upload. Error: ', err);
            uploadStatus.isFailed = true;
            uploadStatus.isSuccess = false;
            uploadStatus.isUploading = false;
            uploadStatus.isValidating = false;
            uploadStatus.isUploadError = false;
            this.setState({ uploadStatus, enableUpload: true, fileName, fileSize, errorMsg: { message: err } });
            this.props.notify({
              type: 'error',
              message: formatMessage(err)
            });
          };
          xhr.onloadstart = () => {
            uploadStatus.isFailed = false;
            uploadStatus.isSuccess = false;
            uploadStatus.isUploading = true;
            uploadStatus.isValidating = false;
            uploadStatus.isUploadError = false;
            this.setState({ uploadStatus, fileName, fileSize, processedPercent: '0' });
          }
          xhr.upload.onprogress = event => {
            // Triggers periodically during the upload
            if (event.lengthComputable) {
              const uploadPercent = Math.ceil((event.loaded / event.total) * 100);
              uploadStatus.isValidating = uploadPercent === 100;
              uploadStatus.isUploading = uploadPercent !== 100;
              this.setState({ uploadPercent, uploadStatus });
            }
          };
          xhr.upload.onerror = () => {
            // non-HTTP error
            console.log('Error in file upload. Error: ', xhr.status);
          };
          xhr.upload.onload = () => {
            // upload finished successfully
            console.log('File uploaded successfully!');
          };
          xhr.send(formData);
        };
      }
    }
  };

  exportTags = () => {
    this.props.toggleLoader(true);
    Request.get(`/v1/projects/${this.projectId}/tags/export/`).then(result => {
      this.props.toggleLoader(false);
      const { isSuccess, response } = result;
      if (isSuccess) {
        this.setState({ enableExport: false });
        this.props.notify({
          type: 'success',
          message: response.message
        });
        this.checkExportStatus();
      } else {
        this.props.notify({
          type: 'error',
          message: formatMessage(response) || 'Error in export tags'
        });
      }
    });
  };

  showUploadModal = () => {
    this.setState({ showUploadModal: true });
  };

  addNewField = () => {
    const { dynamicFields } = this.state;
    dynamicFields.push({
      attribute_name: '',
      attribute_value: '',
      isManuallyAdded: true,
      id: new Date().getTime()
    });
    this.setState({ dynamicFields });
  };

  removeField = field => {
    let { dynamicFields } = this.state;
    dynamicFields = dynamicFields.filter(obj => (obj.id !== field.id));
    this.setState({ dynamicFields });
  };

  toggleGraph = () => {
    this.setState({
      expandGraph: !this.state.expandGraph
    });
  };

  clearTimer = () => {
    if (this.timer) {
      window.clearTimeout(this.timer);
    }
  };

  resetSearch = () => {
    this.clearTimer();
    this.timer = window.setTimeout(() => {
      this.setState({ searchResult: [] });
      this.clearTimer();
    }, 800);
  };

  searchTaxonomy = evt => {
    let searchResult = [];
    let query = this.searchKey.value;
    query = query ? query.trim() : '';
    this.props.toggleLoader(true);
    const url = `/v1/projects/${this.projectId}/tags/search/?search=${query}&autocorrect=true&autocomplete=true&limit=10`;
    Request.get(url).then(result => {
      this.props.toggleLoader(false);
      const { response, isSuccess } = result;
      if (isSuccess) {
        this.setState({ searchResult: response.result || [] });
      } else {
        this.props.notify({
          type: 'error',
          message: formatMessage(response) || 'Error in searching taxonomy'
        });
      }
    });
  };

  updateTab = selectedTab => {
    this.setState({ selectedTab });
  };

  select = tag => {
    const { treeData } = this.state;
    this.fetchTagTree(tag, true);
    this.fetchTag(tag, true);
    this.searchKey.value = '';
    this.setState({ searchResult: [] });
  };

  redirect = link => {
    this.context.router.history.push(link);
  };

  render() {
    const { selectedProject } = this.props;
    const {
      isNew,
      isAdmin,
      fileName,
      fileSize,
      errorMsg,
      formData,
      treeData,
      treeView,
      criteria,
      querySet,
      showModal,
      showGraph,
      parentTag,
      hasAccess,
      warningMsg,
      successMsg,
      activeTags,
      expandGraph,
      selectedTag,
      searchOrder,
      selectedTab,
      searchResult,
      uploadStatus,
      enableUpload,
      uploadPercent,
      enableExport,
      dynamicFields,
      exportPercent,
      userPermission,
      processedPercent,
      showUploadModal,
      showAdvancedSearch
    } = this.state;
    return (
      <div className={styles.taxonomy}>
        {/* Fixed side menu */}
        <aside className={styles.nsidemenu}>
          {this.sideMenu.map(menu => (
              <div className={styles.link} key={menu.name}>
                {menu.link &&
                  <span
                    title={menu.name}
                    role="presentation"
                    onClick={() => { this.redirect(menu.link); }}
                  >
                    <i className={`fa fa-${menu.icon}`} aria-hidden="true" />
                  </span>
                }
              </div>
            ))
          }
        </aside>
        <div className={styles.nmain}>
          {/* Breadcrumb */}
          <div className={styles.breadcrumb}>
            <div className={styles.title}>
              <Breadcrumb
                params={{
                  push: this.context.router.history.push,
                  list: [{
                    link: '/project',
                    name: 'Projects'
                  }, {
                    link: `/project/${this.projectId}`,
                    name: selectedProject.project_name
                  }, {
                    link: '',
                    name: `Term Summary: ${isNew ? 'New Tag' : ((selectedTag.name || selectedTag.tag_name) || '')}`
                  }]
                }}
              />
            </div>
            {/* Graph, upload and download actions */}
            <div className={styles.actions}>
              <div className={styles.rhs}>
                <div
                  title="Toggle graph"
                  role="presentation"
                  onClick={this.toggleView}
                  className={`${styles.graph} ${showGraph ? styles.selected : ''}`}
                >
                  <i className="fa fa-sitemap" aria-hidden="true" />
                </div>
                <div
                  style={{
                    opacity: (hasAccess && enableUpload) ? '1' : '0.5',
                    pointerEvents: (hasAccess && enableUpload) ? 'auto' : 'none'
                  }}
                  title="File upload"
                  role="presentation"
                  className={styles.upload}
                  onClick={this.showUploadModal}
                >
                  <i className="fa fa-upload" aria-hidden="true" />
                  <input
                    className={`${styles.progress} ${enableUpload ? 'hide' : 'show'}`}
                    style={{ width: `${processedPercent}%` }}
                  />
                </div>
                <div
                  style={{
                    opacity: (hasAccess && enableExport) ? '1' : '0.5',
                    pointerEvents: (hasAccess && enableExport) ? 'auto' : 'none'
                  }}
                  title="Export tags"
                  role="presentation"
                  onClick={this.exportTags}
                  className={styles.download}
                >
                  <i className="fa fa-download" aria-hidden="true" />
                  <input
                    className={`${styles.progress} ${enableExport ? 'hide' : 'show'}`}
                    style={{ width: `${exportPercent}%` }}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className={styles.main} id="main-content">
            <div className={styles.nside}>
              <div className={styles.title}>
                {selectedProject.project_name}
              </div>
              {/* Tabs */}
              {/* Hierarchy tab only has the functionlity */}
              {/* Concepts and Label's functionality will be added */}
              <div className={styles.tabs}>
                <div
                  role="presentation"
                  onClick={() => { this.updateTab('hierarchy'); }}
                  className={`${styles.tab1} ${selectedTab === 'hierarchy' ? styles.selected : ''}`}
                >
                  <span className="font-14">
                    <i className="fa fa-bars" aria-hidden="true" />
                    &nbsp;&nbsp;Hierarchy
                  </span>
                </div>
                <div
                  role="presentation"
                  onClick={() => { this.updateTab('concepts'); }}
                  className={`${styles.tab2} ${selectedTab === 'concepts' ? styles.selected : ''}`}
                >
                  <span className="font-14">
                    <i className="fa fa-cog" aria-hidden="true" />
                    &nbsp;&nbsp;Concepts
                  </span>
                </div>
                <div
                  role="presentation"
                  onClick={() => { this.updateTab('labels'); }}
                  className={`${styles.tab3} ${selectedTab === 'labels' ? styles.selected : ''}`}
                >
                  <span className="font-14">
                    <i className="fa fa-tag" aria-hidden="true" />
                    &nbsp;&nbsp;Labels
                  </span>
                </div>
              </div>
              {/* Tags side menu with search */}
              <div className={`${styles.list} slim-scroll`} id="sidemenu">
                <ul className={styles.horscr}>
                  <li className="font-16 font-wt-400">
                    <i
                      aria-hidden="true"
                      role="presentation"
                      title="Add new tag"
                      onClick={this.addNewTag}
                      style={{
                        opacity: hasAccess ? '1' : '0.4',
                        pointerEvents: hasAccess ? 'auto' : 'none'
                      }}
                      className="fa fa-plus-circle"
                    />
                    <div className={styles.search}>
                      <div className="grid-col-16 text-box al-l-m">
                        <div className="font-medium p-l-r-10">
                          <input
                            type="text"
                            placeholder="Search"
                            onBlur={this.resetSearch}
                            onChange={this.searchTaxonomy}
                            ref={value => { this.searchKey = value; }}
                          />
                          <i className={`fa fa-search ${styles.icon}`} aria-hidden="true" />
                        </div>
                        <i
                          aria-hidden="true"
                          role="presentation"
                          onClick={this.showAdvancedSearch}
                          className={`fa fa-cog ${styles.advance}`}
                        />
                        <Suggestions list={searchResult} selectValue={this.select} />
                      </div>
                    </div>
                  </li>
                  {treeData && treeData.map(tag => (
                    <li
                      key={tag.tag_id}
                      id={`tag-${tag.tag_id}`}
                      role="presentation"
                      className="font-16 font-wt-400"
                      onClick={() => {
                        this.fetchTagTree(tag);
                        this.getTag(tag);
                      }}
                    >
                      <i
                        aria-hidden="true"
                        className={`fa fa-caret-${activeTags.indexOf(tag.tag_association_id) === -1 ? 'right' : 'down'}`}
                      />
                      <span
                        style={{
                          borderLeft: !tag.is_approved ? '3px solid #DE6E34' : ''
                        }}
                        className={
                          `${styles.text} ${this.checkToHighlight(tag, selectedTag, activeTags) ? styles.hglt : ''}`
                        }
                      >
                        &nbsp;&nbsp;{tag.name}&nbsp;&nbsp;
                        {polyKeys.indexOf(tag.tag_id) !== -1 &&
                          <i title="poly key" className="fa fa-product-hunt" aria-hidden="true" />
                        }
                      </span>
                      <ul className={`${styles.sublist} ${activeTags.indexOf(tag.tag_association_id) === -1 ? 'hide' : 'show'}`}>
                        <Options
                          tag={tag}
                          styles={styles}
                          hasAccess={hasAccess}
                          activeTags={activeTags}
                          selectedTag={selectedTag}
                          fetchChildTag={this.getTag}
                          addChildTag={this.addChildTag}
                          checkToHighlight={this.checkToHighlight}
                        />
                        {hasAccess &&
                          <li
                            role="presentation"
                            onClick={evt => {
                              evt.stopPropagation();
                              this.addChildTag(tag);
                            }}
                          >
                            <i className="fa fa-plus" aria-hidden="true" />
                            <span className={styles.text}>&nbsp;&nbsp;Add</span>
                          </li>
                        }
                      </ul>
                    </li>
                  ))
                  }
                  {hasAccess &&
                    <li
                      role="presentation"
                      onClick={this.addNewTag}
                      className="font-14 font-wt-400 hide"
                    >
                      <i className="fa fa-plus" aria-hidden="true" />
                      <span className={styles.text}>&nbsp;&nbsp;Add</span>
                    </li>
                  }
                </ul>
              </div>
            </div>
            <div className={styles.ndetail}>
              <div id="content" className={`${styles.content} panel-body slim-scroll`}>
                <div className={`row ${styles.custom_row}`}>
                  <div
                    id="summary"
                    style={{ width: expandGraph ? '3%' : '' }}
                    className={`${styles.summary} ${expandGraph ? 'slideInRight' : 'slideInLeft'}`}
                  >
                    <div className={styles.title}>
                      {!expandGraph &&
                        <span>Term Summary</span>
                      }
                      {showGraph &&
                        <span
                          role="presentation"
                          className={styles.toggle}
                          onClick={this.toggleGraph}
                        >
                          <i className={`fa fa-angle-double-${expandGraph ? 'right' : 'left'}`} aria-hidden="true" />
                          {expandGraph &&
                            <span className={styles.vert}>Term Summary</span>
                          }
                        </span>
                      }
                    </div>
                    {!expandGraph &&
                      <React.Fragment>
                        <div className={styles.form}>
                          {parentTag.name &&
                            <div className={styles.field}>
                              <div className={styles.label}>
                                Parent tag
                              </div>
                              <div className={`inp-elm text-box grid-col-16 al-l-m ${styles.value}`}>
                                <div className="font-medium p-l-r-10">
                                  <input
                                    type="text"
                                    id="parent_tag"
                                    placeholder="Parent tag"
                                    className={styles.readOnly}
                                    onBlur={this.trimFieldValue}
                                    value={parentTag.name || ''}
                                    onChange={this.onChangeFormFields}
                                  />
                                </div>
                              </div>
                            </div>
                          }
                          <div className={styles.field}>
                            <div className={styles.label}>
                              Name
                            </div>
                            <div className={`inp-elm text-box grid-col-16 al-l-m ${styles.value}`}>
                              <div className="font-medium p-l-r-10">
                                <input
                                  id="name"
                                  type="text"
                                  placeholder="Name"
                                  autoComplete="new-name"
                                  value={formData.name || ''}
                                  onBlur={this.trimFieldValue}
                                  onChange={this.onChangeFormFields}
                                />
                              </div>
                            </div>
                          </div>
                          <div className={styles.field}>
                            <div className={styles.label}>
                              Description
                            </div>
                            <div className={`inp-elm text-box grid-col-16 al-l-m ${styles.value}`}>
                              <div className="font-medium p-l-r-10">
                                <input
                                  type="text"
                                  id="description"
                                  placeholder="Description"
                                  autoComplete="new-description"
                                  onBlur={this.trimFieldValue}
                                  value={formData.description || ''}
                                  onChange={this.onChangeFormFields}
                                />
                              </div>
                            </div>
                          </div>
                          {!isNew &&
                            <React.Fragment>
                              {!isEmpty(formData.is_approved) &&
                                <div className={styles.field}>
                                  <div className={styles.label}>
                                    Approved
                                  </div>
                                  <div className={`inp-elm text-box grid-col-16 al-l-m ${styles.value}`}>
                                    <div className="font-medium p-l-r-10">
                                      <input
                                        type="text"
                                        id="is_approved"
                                        placeholder="Approved"
                                        className={styles.readOnly}
                                        onBlur={this.trimFieldValue}
                                        onChange={this.onChangeFormFields}
                                        value={formData.is_approved ? 'Yes' : 'No'}
                                      />
                                    </div>
                                  </div>
                                </div>
                              }
                              {formData.approved_by_name &&
                                <div className={styles.field}>
                                  <div className={styles.label}>
                                    Approved by
                                  </div>
                                  <div className={`inp-elm text-box grid-col-16 al-l-m ${styles.value}`}>
                                    <div className="font-medium p-l-r-10">
                                      <input
                                        type="text"
                                        id="approved_by_name"
                                        placeholder="Approved by"
                                        className={styles.readOnly}
                                        onBlur={this.trimFieldValue}
                                        value={formData.approved_by_name}
                                        onChange={this.onChangeFormFields}
                                      />
                                    </div>
                                  </div>
                                </div>
                              }
                              {formData.added_by_name &&
                                <div className={styles.field}>
                                  <div className={styles.label}>
                                    Added by
                                  </div>
                                  <div className={`inp-elm text-box grid-col-16 al-l-m ${styles.value}`}>
                                    <div className="font-medium p-l-r-10">
                                      <input
                                        type="text"
                                        id="added_by_name"
                                        placeholder="Added by"
                                        className={styles.readOnly}
                                        onBlur={this.trimFieldValue}
                                        value={formData.added_by_name}
                                        onChange={this.onChangeFormFields}
                                      />
                                    </div>
                                  </div>
                                </div>
                              }
                            </React.Fragment>
                          }
                          {dynamicFields && dynamicFields.length > 0 && dynamicFields.map(field => (
                            <div className={styles.field} key={field.id}>
                              <div className={`inp-elm text-box grid-col-16 al-l-m ${styles.elabel}`}>
                                <div className="font-medium p-l-r-10">
                                  <input
                                    type="text"
                                    id={`${field.id}-label`}
                                    placeholder="attribute_name"
                                    value={field.attribute_name || ''}
                                    onBlur={this.trimDynamicFieldValue}
                                    onChange={this.onChangeDynamicFields}
                                  />
                                </div>
                              </div>
                              <div className={`inp-elm text-box grid-col-16 al-l-m ${styles.evalue}`}>
                                <div className="font-medium p-l-r-10">
                                  <input
                                    type="text"
                                    id={`${field.id}-value`}
                                    placeholder="attribute_value"
                                    onBlur={this.trimDynamicFieldValue}
                                    value={field.attribute_value || ''}
                                    onChange={this.onChangeDynamicFields}
                                  />
                                </div>
                              </div>
                              {hasAccess &&
                                <div className={styles.remove}>
                                  <i
                                    aria-hidden="true"
                                    className="fa fa-minus"
                                    onClick={() => { this.removeField(field); }}
                                  />
                                </div>
                              }
                            </div>
                            ))
                          }
                          {hasAccess &&
                            <div className={styles.field}>
                              <div className={styles.label}>
                                <span
                                  role="presentation"
                                  className={`${styles.add} font-14`}
                                  onClick={this.addNewField}
                                >
                                  <i className="fa fa-plus" aria-hidden="true" />&nbsp;&nbsp;Add field
                                </span>
                              </div>
                            </div>
                          }
                        </div>
                        <div className={styles.actions}>
                          <div className={`${styles.each} ${hasAccess ? '' : styles.disabled}`}>
                            <button
                              type="button"
                              onClick={this.createOrUpdateTag}
                              className="btn btn-normal grid-col-16 pad-10-px font-medium"
                            >
                              {isNew ? 'Create' : 'Save'}
                            </button>
                          </div>
                          <div className={`${styles.each} ${(hasAccess && !isNew) ? '' : styles.disabled}`}>
                            <button
                              type="button"
                              onClick={this.disAssociate}
                              className="btn btn-normal grid-col-16 pad-10-px font-medium"
                            >
                              Disassociate
                            </button>
                          </div>
                          <div className={`${styles.each} ${isNew ? '' : styles.disabled}`}>
                            <button
                              type="button"
                              onClick={this.resetOrDeleteTag}
                              className="btn btn-normal grid-col-16 pad-10-px font-medium"
                            >
                              {isNew ? 'Reset' : 'Delete'}
                            </button>
                          </div>
                          <div className={`${styles.each} ${styles.disabled}`}>
                            <button
                              type="button"
                              onClick={this.refreshTag}
                              className="btn btn-normal grid-col-16 pad-10-px font-medium"
                            >
                              Refresh
                            </button>
                          </div>
                          <div
                            className={
                              `${styles.each} ${this.disable(isAdmin, formData.is_approved, isNew) ? styles.disabled : ''}`
                            }
                          >
                            <button
                              type="button"
                              onClick={this.approveTag}
                              className="btn btn-normal grid-col-16 pad-10-px font-medium"
                            >
                              Approve
                            </button>
                          </div>
                        </div>
                      </React.Fragment>
                    }
                  </div>
                  <div
                    id="graph"
                    style={{ width: expandGraph ? '97%' : '' }}
                    className={`${styles.graph} ${expandGraph ? 'slideInRight' : 'slideInLeft'} ${showGraph ? '' : 'hide'}`}
                  >
                  </div>
                </div>
              </div>
            </div>
            <ConfirmOption
              params={{
                showModal,
                handleHide: this.handleHide,
                proceed: this.proceedToDisAssociate
              }}
            />
            <AdvancedSearch
              params={{
                criteria,
                querySet,
                searchOrder,
                showAdvancedSearch,
                remove: this.deleteField,
                addCriteria: this.addCriteria,
                listenChange: this.listenChange,
                applyCriteria: this.applyCriteria,
                handleHide: this.closeAdvancedSearch
              }}
            />
            <Upload
              params={{
                fileName,
                fileSize,
                errorMsg,
                successMsg,
                warningMsg,
                this: this,
                uploadStatus,
                uploadPercent,
                showUploadModal,
                uploadFile: this.uploadFile,
                resetUpload: this.resetUpload,
                cancelUpload: this.cancelUpload,
                handleHide: this.handleUploadHide
              }}
            />
          </div>
        </div>
      </div>
    );
  }
}

Taxonomy.contextTypes = {
  router: PropTypes.shape({}).isRequired
};

Taxonomy.propTypes = {
  notify: PropTypes.func,
  resetValues: PropTypes.func,
  toggleLoader: PropTypes.func,
  fetchCurrentProject: PropTypes.func,
  refreshNotifications: PropTypes.func,
  selectedProject: PropTypes.oneOfType([PropTypes.object])
};

Taxonomy.defaultProps = {
  notify: noFunc,
  selectedProject: {},
  resetValues: noFunc,
  toggleLoader: noFunc,
  fetchCurrentProject: noFunc,
  refreshNotifications: noFunc
};

export default Taxonomy;
