import { put } from 'redux-saga/effects';
import { replace as replaceRouter } from 'react-router-redux';

import { request, storage, store, formatMessage } from '../../utils/common';

/**
 * { generator_description }
 *
 * @param      {<type>}  action  The action
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* signin({ data }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().post('/auth/', data);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    storage().setItem('authToken', response.token);
    yield put({ type: 'SIGN_IN:SUCCESS', ...response });
    yield put(replaceRouter('/home'));
  } else {
    yield put({ type: 'SIGN_IN:FAILED' });
    let error = response.non_field_errors;
    if (!error) {
      error = formatMessage(response) || 'Error in login';
    }
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: error
      }
    });
  }
}

/**
 * { generator_description }
 *
 * @param      {<type>}  action  The action
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* verifyUser({ userId, data }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().put(`/v1/user/${userId}/new/`, data);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'success',
        message: 'User token verified'
      }
    });
    yield put({ type: 'VERIFY:USER:SUCCESS', verifiedUser: response || {} });
    yield put(replaceRouter('/login'));
  } else {
    yield put({ type: 'VERIFY:USER:FAILED', verifiedUser: {} });
    let error = response.non_field_errors;
    if (!error) {
      error = formatMessage(response) || 'Error in verifying the user';
    }
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: error
      }
    });
  }
}

/**
 * { generator_description }
 *
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* refreshToken({ isFromLogin }) {
  const { response, isSuccess } = yield request().post('/auth/refresh-token/', {
    token: storage().getItem('authToken')
  });

  if (isSuccess) {
    storage().setItem('authToken', response.token);
    store().dispatch({ type: 'SESSION_USER:FETCH', isFromLogin });
  } else {
    yield put({ type: 'USER:DO_LOGOUT' });
  }
}

/**
 * { generator_description }
 *
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* logout() {
  storage().removeItem('authToken');
  yield put(replaceRouter('/login'));
}
