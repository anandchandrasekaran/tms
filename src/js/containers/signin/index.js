import { connect } from 'react-redux';
import SignIn from './signin';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  signin: data => {
    dispatch({ type: 'USER:SIGN_IN', data });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
