import Immutable from 'seamless-immutable';

const defaultState = Immutable.flatMap({
  token: '',
  verifiedUser: {}
});

function mergeData(state, data) {
  return Immutable.merge(state, data);
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'SIGN_IN:SUCCESS':
      return mergeData(state, { token: action.token });
    case 'SIGN_IN:FAILED':
      return mergeData(state, { token: '' });
    case 'VERIFY:USER:FAILED':
    case 'VERIFY:USER:SUCCESS':
      return mergeData(state, { verifiedUser: action.data });
    default:
      return state;
  }
};
