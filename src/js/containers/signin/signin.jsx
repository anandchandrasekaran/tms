import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Custom modules and components
import {
  noFunc, isEmpty
} from '../../utils/common';
import styles from './style.scss';
import Banner from '../../components/Banner';

// Images
const fields = ['username', 'password'];
const keyValue = {
  password: 'Password',
  username: 'User name'
};
const mail = 'https://static-dev.vuetag.ai/assets/images/Mail.svg';
const passwordIcon = 'https://static-dev.vuetag.ai/assets/images/Password.svg';

/**
 * This class describes a sign in.
 *
 * @class      SignIn (name)
 */
class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      formData: {}
    };
  }

  trimFieldValue = evt => {
    const { formData } = this.state;
    const { id, value } = evt.target;
    formData[id] = isEmpty(value) ? '' : value.trim();
    this.setState({ formData });
    this.validateField(id, value);
  };

  /**
   * validate
   * Validates the field value on focus out
   */
  validate = evt => {
    const { formData } = this.state;
    const { id, value } = evt.target;
    formData[id] = value;
    this.setState({ formData });
    this.validateField(id, value);
  };

  validateField = (key, value) => {
    let { errors } = this.state;
    if (isEmpty(value)) {
      this.isValid = false;
      errors[key] = `${keyValue[key]} cannot be empty`;
    } else {
      delete errors[key];
    }
    this.setState({ errors });
  };

  /**
   * validatForm
   *
   * Validates all the fields value
   */
  validatForm = formData => {
    fields.forEach(field => {
      this.validateField(field, formData[field]);
    });
  };

  handleSubmit = evt => {
    evt.preventDefault();
    const { formData } = this.state;
    this.isValid = true;
    this.validatForm(formData);
    if (this.isValid) {
      this.props.signin(formData);
    }
  };

  redirect = evt => {
    evt.preventDefault();
    this.context.router.history.push('/signup');
  };

  render() {
    const { formData, errors } = this.state;
    return (
      <div className={styles.login}>
        <Banner />
        <div id="signinForm" className={styles.container}>
          <div className={styles.inner}>
            <form className={styles.form} onSubmit={this.handleSubmit}>
              <p>MSD Taxonomy</p>
              <p className="font-xlarge fw-500 color-2">
                Sign In
              </p>
              <div className="inp-elm text-box grid-col-16 al-l-m font-small">
                <div className="font-medium p-l-r-10">
                  <img alt="mail" src={mail} />
                  <input
                    type="text"
                    id="username"
                    autoComplete="username"
                    onChange={this.validate}
                    value={formData.username || ''}
                    onBlur={this.trimFieldValue}
                    placeholder="User name"
                  />
                </div>
                {errors && errors.username &&
                  <span className="color-6">{errors.username}</span>
                }
              </div>
              <div className="inp-elm pass-box grid-col-16 al-l-m font-small">
                <div className="font-medium p-l-r-10">
                  <img alt="password" src={passwordIcon} />
                  <input
                    id="password"
                    type="password"
                    placeholder="Password"
                    onChange={this.validate}
                    onBlur={this.trimFieldValue}
                    autoComplete="new-password"
                    value={formData.password || ''}
                  />
                </div>
                {errors && errors.password &&
                  <span className="color-6">{errors.password}</span>
                }
              </div>
              <div className={`${styles.submit} grid-col-16 al-c-m`}>
                <button type="submit" className="btn btn-gradient grid-col-16 pad-10-px font-medium">
                  Sign In
                </button>
              </div>
              <div className={`${styles.submit} grid-col-16 al-c-m`}>
                <button
                  type="button"
                  onClick={this.redirect}
                  className="btn btn-normal grid-col-16 pad-10-px font-medium"
                >
                  Sign Up
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

SignIn.contextTypes = {
  router: PropTypes.shape({}).isRequired
};

SignIn.propTypes = {
  signin: PropTypes.func
};

SignIn.defaultProps = {
  signin: noFunc
};

export default SignIn;
