import { connect } from 'react-redux';
import Users from './user';

const mapStateToProps = state => ({
  users: state.user.users,
  currentUser: state.user.info,
  closeModal: state.user.closeModal
});

const mapDispatchToProps = dispatch => ({
  loadUsers: showLoader => {
    dispatch({ type: 'USER_LIST:FETCH', showLoader });
  },
  resetModalState: () => {
    dispatch({ type: 'USER:MODAL:RESET' });
  },
  inviteUser: data => {
    dispatch({ type: 'INVITE:USER', data });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Users);
