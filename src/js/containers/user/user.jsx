import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
// custom components
import styles from './style.scss';
import { noFunc } from '../../utils/common';
import GridHeader from './components/gridHeader';
import InviteUser from './components/inviteUser';
import Paginator from '../../components/Paginator';
import Breadcrumb from '../../components/Breadcrumb';

class Users extends Component {
  constructor(props) {
    super(props);
    this.sideMenu = [{
      name: 'default',
      link: '',
      icon: ''
    }, {
      name: 'Projects',
      link: '/project',
      icon: 'home'
    }, {
      name: 'Users',
      link: '/user',
      icon: 'users'
    }, {
      name: 'Notifications',
      link: '/notifications',
      icon: 'envelope'
    }];
    this.state = {
      page: 1,
      limit: 10,
      showModal: false
    };
  }

  componentDidMount() {
    this.fetchUsers();
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.closeModal && this.props.closeModal) {
      this.handleHide(true);
    }
  }

  fetchUsers = () => {
    const { page, limit } = this.state;
    this.props.loadUsers({ page, limit });
  };

  /**
   * handleHide
   *
   * @return     {<type>}  Manages to hide and reset the state values on modal close
   */
  handleHide = fetchData => {
    this.setState({
      showModal: false
    }, () => {
      if (fetchData) {
        this.props.resetModalState();
        this.fetchUsers();
      }
    });
  };

  /**
   * Shows the modal.
   *
   * @return     {<type>}  { description_of_the_return_value }
   */
  showModal = () => {
    this.setState({ showModal: true });
  };

  inviteUser = data => {
    this.props.inviteUser(data);
  };

  /**
   * `goTo`
   * It is used in pagination. When the user clicks on each pagination cell, it will make request to fetch images
   */
  goTo = page => {
    if (this.state.page !== page) {
      this.setState({
        page
      }, () => {
        this.fetchUsers();
      });
    }
  };

  loadData = () => {
    const limit = document.getElementById('pagelimit').value;
    this.setState({
      limit,
      page: 1
    }, () => {
      this.fetchUsers();
    });
  };

  redirect = link => {
    this.context.router.history.push(link);
  };

  render() {
    const { users, currentUser } = this.props;
    const { showModal, page, limit } = this.state;
    const totalUsers = users.total_records;
    return (
      <div className={styles.users}>
        <aside className={styles.nsidemenu}>
          {this.sideMenu.map(menu => (
              <div className={styles.link} key={menu.name}>
                {menu.link &&
                  <span
                    title={menu.name}
                    role="presentation"
                    onClick={() => { this.redirect(menu.link); }}
                  >
                    <i className={`fa fa-${menu.icon}`} aria-hidden="true" />
                  </span>
                }
              </div>
            ))
          }
        </aside>
        <section id="main-content" className={styles.mcontent}>
          <div className={styles.breadcrumb}>
            <div className={styles.title}>
              <Breadcrumb
                params={{
                  push: this.context.router.history.push,
                  list: [{
                    link: '/project',
                    name: 'Projects'
                  }, {
                    link: '',
                    name: 'Users'
                  }]
                }}
              />
            </div>
            <div className={styles.actions}>
              <div className={styles.rhs}>
                {currentUser.isAdmin &&
                  <div
                    role="presentation"
                    title="Invite user"
                    className={styles.add}
                    onClick={this.showModal}
                  >
                    <i className="fa fa-user-plus" aria-hidden="true" />
                  </div>
                }
              </div>
            </div>
          </div>
          <div className={`${styles.content} slim-scroll`}>
            {(totalUsers > 0) &&
              <Fragment>
                <div className={styles.row}>
                  <div className={styles.header}>
                    {/* TODO: Enable sort option in the header component */}
                    <GridHeader styles={styles.field} />
                  </div>
                  {users.list &&
                    users.list.map(user => (
                      <div
                        key={user.id}
                        role="presentation"
                        className={styles.field_row}
                      >
                        <div className={`${styles.field} font-16`}>
                          {user.id}
                        </div>
                        <div className={`${styles.field} font-16`}>
                          {user.name}
                        </div>
                        <div className={`${styles.field} font-16`}>
                          {user.role.name}
                        </div>
                        <div className={styles.field}>
                          <div>
                            <section>
                              <p className="font-16 color-1" title={user.created_date}>
                                {moment(user.created_date).format('DD MMM YYYY HH:mm:ss')}
                              </p>
                            </section>
                          </div>
                        </div>
                        <div className={`${styles.field} font-16`}>
                          {moment(user.modified_date).format('DD MMM YYYY HH:mm:ss')}
                        </div>
                      </div>
                    ))
                  }
                </div>
                <Paginator
                  params={{
                    page,
                    limit,
                    keyboard: true,
                    goTo: this.goTo,
                    show: totalUsers > 0,
                    total: totalUsers || 0,
                    loadData: this.loadData,
                    style: { bottom: (limit === 10) ? 0 : '' }
                  }}
                />
              </Fragment>
            }
            {(totalUsers === 0) &&
              <div className={styles.empty}>
                <p className="theme-color font-wt-600 font-18">
                  No user created
                </p>
              </div>
            }
          </div>
          <InviteUser
            params={{
              showModal,
              create: this.inviteUser,
              handleHide: this.handleHide
            }}
          />
        </section>
      </div>
    );
  }
}

Users.contextTypes = {
  router: PropTypes.shape({}).isRequired
};

Users.propTypes = {
  loadUsers: PropTypes.func,
  closeModal: PropTypes.bool,
  inviteUser: PropTypes.func,
  resetModalState: PropTypes.func,
  users: PropTypes.oneOfType([PropTypes.object]),
  currentUser: PropTypes.oneOfType([PropTypes.object])
};

Users.defaultProps = {
  users: {},
  currentUser: {},
  loadUsers: noFunc,
  closeModal: false,
  inviteUser: noFunc,
  resetModalState: noFunc
};

export default Users;
