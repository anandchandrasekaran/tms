import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, ModalHeader, ModalBody } from 'msd-modal';
// custom components
import styles from './style.scss';
import { isEmpty, validateEmail } from '../../../../utils/common';

const keyValue = {
  email: 'Email'
};
const fields = ['email'];
const close = 'https://static-dev.vuetag.ai/assets/images/close.svg';

class InviteUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      formData: {}
    };
  }

  trimFieldValue = evt => {
    const { formData } = this.state;
    const { id, value } = evt.target;
    formData[id] = isEmpty(value) ? '' : value.trim();
    this.setState({ formData });
    this.validateField(id, value);
  };

  /**
   * validate
   * Validates the field value on focus out
   */
  validate = evt => {
    const { formData } = this.state;
    const { id, value } = evt.target;
    formData[id] = value;
    this.setState({ formData });
    this.validateField(id, value);
  };

  validateField = (key, value) => {
    let { errors } = this.state;
    if (isEmpty(value)) {
      this.isValid = false;
      errors[key] = `${keyValue[key]} cannot be empty`;
    } else if (validateEmail(value)) {
      this.isValid = false;
      errors[key] = 'Email format is not valid';
    } else {
      delete errors[key];
    }
    this.setState({ errors });
  };

  /**
   * validatForm
   *
   * Validates all the fields value
   */
  validatForm = formData => {
    fields.forEach(field => {
      this.validateField(field, formData[field]);
    });
  };

  inviteUser = evt => {
    evt.preventDefault();
    const { formData } = this.state;
    this.isValid = true;
    this.validatForm(formData);
    if (this.isValid) {
      this.props.params.create(formData);
    }
  };

  closeModal = () => {
    this.setState({ errors: {}, formData: {} });
    this.props.params.handleHide(false);
  };

  render() {
    const { params } = this.props;
    const { errors, formData } = this.state;
    return (
      <Modal
        tag="div"
        showCloseIcon={false}
        isOpen={params.showModal}
        className={styles.inviteuser}
      >
        <ModalHeader tag="div" className={styles.header}>
          <span className={styles.title}>
            Invite User
          </span>
          <div role="presentation" className={styles.close} onClick={this.closeModal}>
            <img src={close} alt="close" />
          </div>
        </ModalHeader>
        <ModalBody tag="div" className={styles.content}>
          <section>
            <form onSubmit={this.inviteuser}>
              <div className={styles.common}>
                <div className={styles.create}>
                  <div className={styles.desc}>
                    <div className="inp-elm pass-box grid-col-16 al-l-m font-small">
                      <div className="font-medium p-l-r-10">
                        <input
                          id="email"
                          type="text"
                          placeholder="Email"
                          onChange={this.validate}
                          onBlur={this.trimFieldValue}
                          value={formData.email || ''}
                        />
                      </div>
                      {errors.email &&
                        <span className="color-6">{errors.email}</span>
                      }
                    </div>
                  </div>
                  <div className="text-center">
                    <button
                      type="submit"
                      onClick={this.inviteUser}
                      className="btn btn-primary font-wt-600"
                    >
                      Invite User
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </section>
        </ModalBody>
      </Modal>
    );
  }
}

InviteUser.propTypes = {
  params: PropTypes.oneOfType([
    PropTypes.object
  ])
};

InviteUser.defaultProps = {
  params: null
};

export default InviteUser;
