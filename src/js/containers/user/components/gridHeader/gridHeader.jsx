import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

class GridHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: [
        {
          id: 'id',
          name: 'Id',
          sort: false
        },
        {
          id: 'name',
          name: 'Name',
          sort: false
        },
        {
          id: 'role',
          name: 'Role',
          sort: false
        },
        {
          id: 'created_time',
          name: 'Created Time',
          sort: false
        },
        {
          id: 'modified_time',
          name: 'Modified Time',
          sort: false
        }
      ]
    };
  }

  render() {
    const { styles } = this.props;
    const { fields } = this.state;
    return (
      <Fragment>
        {fields.map(field => (
          <div key={field.id} className={styles}>
            <span>{field.name}</span>
            {field.sort &&
              <i className="fa fa-sort" aria-hidden="true" />
            }
          </div>
          ))
        }
      </Fragment>
    );
  }
}

GridHeader.propTypes = {
  styles: PropTypes.string
};

GridHeader.defaultProps = {
  styles: ''
};

export default GridHeader;
