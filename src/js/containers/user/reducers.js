import Immutable from 'seamless-immutable';

const defaultState = Immutable.flatMap({
  info: {},
  users: {},
  roles: [],
  notifications: {},
  closeModal: false
});

function mergeData(state, data) {
  return Immutable.merge(state, data);
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'USER:FETCH:SUCCESS':
    case 'SESSION_USER:FETCH:SUCCESS':
      return mergeData(state, { info: action.user });

    case 'USER:FETCH:FAILED':
    case 'SESSION_USER:FETCH:FAILED':
      return mergeData(state, { info: {} });

    case 'USER_LIST:FETCH:FAILED':
    case 'USER_LIST:FETCH:SUCCESS':
      return mergeData(state, { users: action.users || {} });
    case 'INVITE:USER:SUCCESS':
    case 'INVITE:USER:FAILED':
      return mergeData(state, { closeModal: true });

    case 'ROLES:FETCH:FAILED':
    case 'ROLES:FETCH:SUCCESS':
      return mergeData(state, { roles: action.roles || [] });

    case 'NOTIFICATIONS:FETCH:FAILED':
    case 'NOTIFICATIONS:FETCH:SUCCESS':
      return mergeData(state, { notifications: action.data || [] });

    case 'USER:MODAL:RESET':
      return mergeData(state, { closeModal: false });

    default:
      return state;
  }
};
