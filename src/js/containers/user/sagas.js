import { put } from 'redux-saga/effects';
import { replace as replaceRouter } from 'react-router-redux';

import { isEmpty, request, storage, formatMessage } from '../../utils/common';

/**
 * checkIsAdmin
 *
 * @param      {<type>}   role    The role
 * @return     {boolean}  { description_of_the_return_value }
 */
const checkIsAdmin = role => {
  let isAdmin = false;
  if (role && !isEmpty(role.name)) {
    isAdmin = role.name.toLowerCase() === 'admin';
  }
  return isAdmin;
};

/**
 * Fetches the current user by session
 *
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* getCurrentUser({ isFromLogin }) {
  const { response, isSuccess } = yield request().get('/v1/user/');
  if (isSuccess) {
    const user = response.user || {};
    user.role = response.role || {};
    user.isAdmin = checkIsAdmin(user.role);
    user.org = response.org || {};
    yield put({ user, type: 'SESSION_USER:FETCH:SUCCESS' });
    if (isFromLogin) {
      yield put(replaceRouter('/home'));
    }
  } else {
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in getting current user detail'
      }
    });
  }
}

/**
 * Fetch single user detail
 *
 * @param      {<type>}  action  The action
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* get({ userId }) {
  const { response, isSuccess } = yield request().get(`/v1/user/${userId}`);

  if (isSuccess) {
    yield put({ type: 'USER:FETCH:SUCCESS', user: response || {} });
  } else {
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in getting user detail'
      }
    });
  }
}

/**
 * getUsers
 *
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* getUsers() {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().get('/v1/users/');

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    const formatted = [];
    const users = response.user_list || [];
    if (users.length > 0) {
      users.forEach(obj => {
        const user = obj.user || {};
        if (!isEmpty(user.name)) {
          user.role = obj.role || {};
          user.isAdmin = checkIsAdmin(user.role);
          formatted.push(user);
        }
      });
    }
    yield put({ type: 'USER_LIST:FETCH:SUCCESS', users: { list: formatted, total_records: response.total_records } });
  } else {
    yield put({ type: 'USER_LIST:FETCH:FAILED', users: [] });
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in getting users list'
      }
    });
  }
}

/**
 * { generator_description }
 *
 * @param      {<type>}  action  The action
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* inviteUser({ data }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().post('/v1/user/', data);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'success',
        message: 'New user invited'
      }
    });
    yield put({ type: 'INVITE:USER:SUCCESS', users: response.result || {} });
  } else {
    yield put({ type: 'INVITE:USER:FAILED' });
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in inviting new user'
      }
    });
  }
}

/**
 * Loads the roles
 *
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* getRoles() {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().get('/v1/roles/');

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({ type: 'ROLES:FETCH:SUCCESS', roles: response.result || {} });
  } else {
    yield put({ type: 'ROLES:FETCH:FAILED' });
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in loading roles'
      }
    });
  }
}

/**
 * { generator_description }
 *
 * @param      {<type>}  action  The action
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* mapUsers({ data, projectId }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().patch(`/v1/projects/${projectId}/`, data);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'success',
        message: response.message
      }
    });
    yield put({ type: 'MAP:USER:SUCCESS', result: response.result || {} });
    yield put({ type: 'PROJECT_USERS:FETCH', projectId });
    yield put({ type: 'PROJECT_NON_USERS:FETCH', projectId });
  } else {
    yield put({ type: 'MAP:USER:FAILED' });
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in mapping users to a project'
      }
    });
  }
}

/**
 * { generator_description }
 *
 * @param      {<type>}  action  The action
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* signup({ data }) {
  // Auth-token is not required for signup action
  storage().removeItem('authToken');

  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().post('/v1/users/', data);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    // No handler defined for success action
    // yield put({ type: 'SIGN_UP:SUCCESS', ...response });
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'success',
        message: 'Signup successfull'
      }
    });
    yield put(replaceRouter('/login'));
  } else {
    let error = response.non_field_errors;
    if (!error) {
      error = formatMessage(response) || 'Error in signup';
    }
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: error
      }
    });
  }
}
