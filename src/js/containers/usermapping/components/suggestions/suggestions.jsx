import React from 'react';
import PropTypes from 'prop-types';
// custom files
import styles from './style.scss';
import { noFunc } from '../../../../utils/common';

const Suggestions = ({ list, selectValue }) => (
  <ul className={`slim-scroll ${styles.list} ${list.length === 0 ? 'hide' : ''}`}>
    {list.map(r => (
      <li
        key={r.id}
        value={r.id}
        role="presentation"
        onClick={selectValue}
        className={styles.item}
      >
        {r.name}
      </li>
    ))}
  </ul>
);

Suggestions.propTypes = {
  selectValue: PropTypes.func,
  list: PropTypes.arrayOf(PropTypes.object)
};

Suggestions.defaultProps = {
  list: [],
  selectValue: noFunc
};

export default Suggestions;
