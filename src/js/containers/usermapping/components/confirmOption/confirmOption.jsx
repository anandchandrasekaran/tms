import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, ModalHeader, ModalBody } from 'msd-modal';
// custom components
import merge from './merge.png';
import styles from './style.scss';
import { isEmpty } from '../../../../utils/common';

const close = 'https://static-dev.vuetag.ai/assets/images/close.svg';

class ConfirmOption extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  yes = () => {
    this.props.params.proceed(null, false);
  };

  closeModal = () => {
    this.props.params.handleHide();
  };

  render() {
    const { params } = this.props;
    return (
      <Modal
        tag="div"
        showCloseIcon={false}
        isOpen={params.showModal}
        className={styles.upload}
      >
        <ModalHeader
          tag="div"
          className={styles.header}
        >
          <span className={styles.title}>Select Option</span>
          <div
            role="presentation"
            className={styles.close}
            onClick={this.closeModal}
          >
            <img src={close} alt="close" />
          </div>
        </ModalHeader>
        <ModalBody tag="div" className={styles.content}>
          <section>
            <div className={styles.common}>
              {/* UI code which handles the UI before upload starts */}
              <div className={styles.create}>
                <div className={styles.desc}>
                  <div className="grid-col-16 al-l-m font-small">
                    <img alt="merge.png" src={merge} />
                  </div>
                  <div className="grid-col-16 al-l-m font-small">
                    <p>
                      Disapproving of this tag will also delete it's child and associations.
                    </p>
                    <p className="font-14">
                      Do you want to continue?
                    </p>
                  </div>
                </div>
                <div className="text-center">
                  <button
                    type="button"
                    onClick={this.closeModal}
                    className="btn font-wt-600"
                  >
                    No
                  </button>
                  <button
                    type="button"
                    onClick={this.yes}
                    className="btn btn-primary font-wt-600"
                  >
                    Yes
                  </button>
                </div>
              </div>
            </div>
          </section>
        </ModalBody>
      </Modal>
    );
  }
}

ConfirmOption.propTypes = {
  params: PropTypes.oneOfType([
    PropTypes.object
  ])
};

ConfirmOption.defaultProps = {
  params: null
};

export default ConfirmOption;
