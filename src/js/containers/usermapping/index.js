import { connect } from 'react-redux';
import UserMapping from './userMapping';

const mapStateToProps = state => ({
  project: state.projects.project,
  projectRoles: state.projects.roles,
  selectedProject: state.projects.project,
  projectUsers: state.projects.projectUsers,
  nonProjectUsers: state.projects.nonProjectUsers
});

const mapDispatchToProps = dispatch => ({
  fetchCurrentProject: projectId => {
    dispatch({ type: 'PROJECT:FETCH', projectId });
  },
  mapUsers: (projectId, data) => {
    dispatch({ type: 'MAP:USER', projectId, data });
  },
  fetchProjectUsers: projectId => {
    dispatch({ type: 'PROJECT_USERS:FETCH', projectId });
  },
  toggleLoader: show => {
    dispatch({ type: `LOADER:${show ? 'SHOW' : 'HIDE'}` });
  },
  fetchNonProjectUsers: projectId => {
    dispatch({ type: 'PROJECT_NON_USERS:FETCH', projectId });
  },
  getProjectRoles: () => {
    dispatch({ type: 'PROJECT_ROLES:FETCH' });
  },
  updateUserPermission: (projectId, data) => {
    dispatch({ type: 'PROJECT_PERMISSION:UPDATE', projectId, data });
  },
  disAssociateUser: (projectId, userId) => {
    dispatch({ type: 'DISASSOCIATE:USER', projectId, userId });
  },
  notify: notification => {
    dispatch({ type: 'NOTIFIER:SHOW', notification });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(UserMapping);
