import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
// custom modules and components
import styles from './style.scss';
import Request from '../../services/request';
import Suggestions from './components/suggestions';
import Paginator from '../../components/Paginator';
import Breadcrumb from '../../components/Breadcrumb';
import ConfirmOption from './components/confirmOption';
import { getParameterValuesFromHash } from '../../utils/routerHooks';
import { noFunc, isEmpty, validateEmail, formatMessage } from '../../utils/common';

const header = {
  audit: ['User', 'Time'],
  tags: ['Name', 'Added by', 'Description', 'Action']
};
const close = 'https://static-dev.vuetag.ai/assets/images/close.svg';

class UserMapping extends Component {
  constructor(props) {
    super(props);
    this.selectedTag = null;
    this.projectId = getParameterValuesFromHash('/project/:projectId').projectId;
    this.sideMenu = [{
      name: 'default',
      link: '',
      icon: ''
    }, {
      name: 'Projects',
      link: '/project',
      icon: 'home'
    }, {
      name: 'Users',
      link: '/user',
      icon: 'users'
    }, {
      name: 'Notifications',
      link: '/notifications',
      icon: 'envelope'
    }];
    this.state = {
      tabs: {
        user: true,
        tags: false,
        audit: false
      },
      page: 1,
      tags: {},
      limit: 10,
      audits: {},
      isAdmin: false,
      showModal: false,
      searchResult: [],
      enableAdd: false,
      userPermission: {}
    };
  }

  componentDidMount() {
    this.props.fetchCurrentProject(this.projectId);
    this.props.fetchProjectUsers(this.projectId, true);
    this.props.fetchNonProjectUsers(this.projectId, false);
    this.props.getProjectRoles();
    this.fetchUserPermission();
  }

  fetchUserPermission = () => {
    this.props.toggleLoader(true);
    Request.get(`/v1/projects/${this.projectId}/user/`).then(result => {
      this.props.toggleLoader(false);
      const { response, isSuccess } = result;
      if (isSuccess) {
        const { permission } = response;
        this.setState({
          userPermission: response || {},
          isAdmin: permission && permission.toLowerCase() === 'admin'
        });
      } else {
        this.props.notify({
          type: 'error',
          message: formatMessage(response) || 'Error in fetching user permission'
        });
      }
    });
  };

  fetchUnapprovedTags = () => {
    this.setState({
      page: 1,
      limit: 10
    }, () => {
      this.loadUnapprovedTags();
    });
  };

  loadUnapprovedTags = () => {
    const { tabs, limit, page } = this.state;
    tabs.user = false;
    tabs.tags = true;
    tabs.audit = false;
    this.setState({ tabs });
    this.props.toggleLoader(true);
    Request.get(`/v1/projects/${this.projectId}/tag_approval/?page_no=${page}&limit=${limit}`).then(result => {
      this.props.toggleLoader(false);
      const { isSuccess, response } = result;
      if (isSuccess) {
        this.setState({ tags: response });
      }
    });
  };

  fetchProjectAudits = () => {
    this.setState({
      page: 1,
      limit: 10
    }, () => {
      this.loadProjectAudits();
    });
  };

  loadProjectAudits = () => {
    const { tabs, page, limit } = this.state;
    tabs.user = false;
    tabs.tags = false;
    tabs.audit = true;
    this.setState({ tabs });
    this.props.toggleLoader(true);
    Request.get(`/v1/audit/projects/${this.projectId}/?limit=${limit}&page_no=${page}`).then(result => {
      this.props.toggleLoader(false);
      const { isSuccess, response } = result;
      if (isSuccess) {
        this.setState({ audits: response });
      }
    });
  };

  handleInputChange = () => {
    let searchResult = [];
    let query = this.search.value;
    query = query ? query.trim() : '';
    if (this.filteredList.length > 0) {
      searchResult = this.filteredList.filter(user => (
        user.name.toLowerCase().indexOf(query) !== -1 || user.email.toLowerCase().indexOf(query) !== -1
      ));
      let enableAdd = false;
      if (searchResult.length === 0 && !validateEmail(query)) {
        enableAdd = true;
      }
      this.setState({ searchResult, enableAdd });
    } else {
      let enableAdd = false;
      if (!isEmpty(query)) {
        enableAdd = !validateEmail(query);
      }
      this.setState({ enableAdd });
    }
  };

  select = evt => {
    const userId = evt.target.value;
    if (userId) {
      this.props.mapUsers(this.projectId, { users_id: [userId] });
    }
    this.search.value = '';
    this.resetSearch();
    this.setState({ searchResult: [] });
  };

  clearTimer = () => {
    if (this.timer) {
      window.clearTimeout(this.timer);
    }
  };

  resetSearch = () => {
    this.clearTimer();
    this.timer = window.setTimeout(() => {
      this.setState({ searchResult: [] });
      this.clearTimer();
    }, 800);
  };

  showSuggestion = () => {
    const users = [];
    const { nonProjectUsers } = this.props;
    if (nonProjectUsers && nonProjectUsers.length > 0) {
      nonProjectUsers.forEach(user => {
        const obj = {
          id: user.id,
          name: user.name,
          email: user.email,
          is_active: user.is_active
        };
        if (!user.is_active) {
          obj.name = user.email;
        }
        users.push(obj);
      });
    }
    this.filteredList = users;
    this.setState({ searchResult: users });
  };

  disAssociateUser = user => {
    const { isAdmin } = this.state;
    if (isAdmin) {
      this.props.disAssociateUser(this.projectId, user.user_id);
    }
  };

  updateUserPermission = (user, access) => {
    const { isAdmin } = this.state;
    const { permission, user_id } = user;
    if (isAdmin && permission.toLowerCase() !== access.toLowerCase()) {
      this.props.updateUserPermission(this.projectId, {
        user_id,
        permission: access.toUpperCase()
      });
    }
  };

  addUser = () => {
    if (this.search.value) {
      const email = this.search.value.trim();
      if (!validateEmail(email)) {
        // First level of tag creation
        this.props.toggleLoader(true);
        Request.post('/v1/user/', { email }).then(result => {
          const { response, isSuccess } = result;
          const { user } = response;
          if (isSuccess && user && !isEmpty(user.id)) {
            this.props.mapUsers(this.projectId, { users_id: [user.id] });
            this.search.value = '';
            this.setState({ enableAdd: false });
          } else {
            this.props.notify({
              type: 'error',
              message: formatMessage(response) || 'Error in adding user'
            });
          }
        });
      }
    }
  };

  redirect = () => {
    this.context.router.history.push(`/project/${this.projectId}/taxonomy`);
  };

  showProjectUsers = () => {
    const { tabs } = this.state;
    tabs.user = true;
    tabs.tags = false;
    tabs.audit = false;
    this.setState({ tabs });
  };

  /**
   * handleHide
   *
   * @return     {<type>}  Manages to hide and reset the state values on modal close
   */
  handleHide = () => {
    this.selectedTag = null;
    this.setState({ showModal: false });
  };

  /**
   * Shows the modal.
   *
   * @return     {<type>}  { description_of_the_return_value }
   */
  showModal = tagId => {
    this.selectedTag = tagId;
    this.setState({ showModal: true });
  };

  approveTag = (tag, isToApprove) => {
    let requestPayload = {};
    if (isToApprove) {
      requestPayload = { tag_ids: [tag.id] };
    } else if (!isEmpty(this.selectedTag)) {
      requestPayload = { disapprove_tag_ids: [this.selectedTag] };
    }
    this.props.toggleLoader(true);
    Request.post(`/v1/projects/${this.projectId}/tag_approval/`, requestPayload).then(result => {
      this.props.toggleLoader(false);
      if (!isToApprove) {
        this.handleHide();
      }
      const { isSuccess, response } = result;
      if (isSuccess) {
        this.props.notify({
          type: 'success',
          message: formatMessage(response)
        });
        this.fetchUnapprovedTags();
      } else {
        this.props.notify({
          type: 'error',
          message: formatMessage(response) || 'Error in approving tags'
        });
      }
    });
  };

  goTo = pageNo => {
    const { page, tabs } = this.state;
    if (page !== pageNo) {
      this.setState({
        page: pageNo
      }, () => {
        if (tabs.audit) {
          this.loadProjectAudits();
        } else if (tabs.tags) {
          this.loadUnapprovedTags();
        }
      });
    }
  };

  loadData = () => {
    const { tabs } = this.state;
    const limit = document.getElementById('pagelimit').value;
    this.setState({
      limit,
      page: 1
    }, () => {
      if (tabs.audit) {
        this.loadProjectAudits();
      } else if (tabs.tags) {
        this.loadUnapprovedTags();
      }
    });
  };

  navigateTo = link => {
    this.context.router.history.push(link);
  };

  render() {
    const {
      tabs,
      page,
      tags,
      limit,
      audits,
      isAdmin,
      showModal,
      enableAdd,
      searchResult
    } = this.state;
    const { project, projectUsers } = this.props;
    return (
      <div className={styles.usermapping}>
        <aside className={styles.nsidemenu}>
          {this.sideMenu.map(menu => (
              <div className={styles.link} key={menu.name}>
                {menu.link &&
                  <span
                    title={menu.name}
                    role="presentation"
                    onClick={() => { this.navigateTo(menu.link); }}
                  >
                    <i className={`fa fa-${menu.icon}`} aria-hidden="true" />
                  </span>
                }
              </div>
            ))
          }
        </aside>
        <section id="main-content" className={styles.mcontent}>
          <div className={styles.breadcrumb}>
            <div className={styles.title}>
              <Breadcrumb
                params={{
                  push: this.context.router.history.push,
                  list: [{
                    link: '/project',
                    name: 'Projects'
                  }, {
                    link: '',
                    name: project.project_name
                  }]
                }}
              />
            </div>
          </div>
          <div className={styles.content}>
            <div className={styles.sidebar}>
              <div className={styles.title}>
                <strong>General</strong>
              </div>
              <section>
                <div className={styles.list}>
                  <div
                    role="presentation"
                    className={`${styles.item} ${tabs.user ? styles.selected : ''}`}
                    onClick={this.showProjectUsers}
                  >
                    <span>
                      <i className="fa fa-user-plus" aria-hidden="true" />
                      &nbsp;&nbsp;User and group access
                    </span>
                  </div>
                  {isAdmin &&
                    <div
                      role="presentation"
                      className={`${styles.item} ${tabs.audit ? styles.selected : ''}`}
                      onClick={this.fetchProjectAudits}
                    >
                      <span>
                        <i className="fa fa-file-o" aria-hidden="true" />
                        &nbsp;&nbsp;Activity log
                      </span>
                    </div>
                  }
                  {isAdmin &&
                    <div
                      role="presentation"
                      className={`${styles.item} ${tabs.tags ? styles.selected : ''}`}
                      onClick={this.fetchUnapprovedTags}
                    >
                      <span>
                        <i className="fa fa-tags" aria-hidden="true" />
                        &nbsp;&nbsp;Unapproved tags
                      </span>
                    </div>
                  }
                  <div role="presentation" className={styles.item} onClick={this.redirect}>
                    <span>
                      <i className="fa fa-bars" aria-hidden="true" />
                      &nbsp;&nbsp;Taxonomy
                    </span>
                  </div>
                </div>
              </section>
            </div>
            <div className={`${styles.row} slim-scroll`}>
              {tabs.user &&
                <Fragment>
                  <div className="pass-box font-small wid-25">
                    <div className="font-medium p-l-r-10">
                      <input
                        type="text"
                        placeholder="Search"
                        onBlur={this.resetSearch}
                        onFocus={this.showSuggestion}
                        onChange={this.handleInputChange}
                        ref={value => { this.search = value; }}
                      />
                    </div>
                    <Suggestions list={searchResult} selectValue={this.select} />
                  </div>
                  <div className={`${styles.add} ${enableAdd ? '' : styles.disable}`}>
                    <button
                      type="button"
                      title="Add user"
                      onClick={this.addUser}
                      className="file-upload btn btn-primary"
                    >
                      <i className="fa fa-user-plus" aria-hidden="true" />
                    </button>
                  </div>
                  {projectUsers &&
                    projectUsers.map(user => (
                      <div
                        key={user.user_id}
                        role="presentation"
                        className={styles.field_row}
                      >
                        <div className={`${styles.field} font-16 font-wt-600`}>
                          {user.user_id}
                        </div>
                        <div className={`${styles.field} font-16 font-wt-600`}>
                          {user.user_name || user.email}
                        </div>
                        <div className={`${styles.field} ${isAdmin ? '' : styles.half}`}>
                          <div>
                            <section>
                              <p className="font-16 color-1 font-wt-600">
                                <span
                                  role="presentation"
                                  onClick={() => {
                                    if (isAdmin) {
                                      this.updateUserPermission(user, 'read');
                                    }
                                  }}
                                  className={user.permission.toLowerCase() === 'read' ? styles.highlight : ''}
                                >
                                  Read
                                </span>
                                <span
                                  role="presentation"
                                  onClick={() => {
                                    if (isAdmin) {
                                      this.updateUserPermission(user, 'write');
                                    }
                                  }}
                                  className={user.permission.toLowerCase() === 'write' ? styles.highlight : ''}
                                >
                                  Write
                                </span>
                                <span
                                  role="presentation"
                                  onClick={() => {
                                    if (isAdmin) {
                                      this.updateUserPermission(user, 'admin');
                                    }
                                  }}
                                  className={user.permission.toLowerCase() === 'admin' ? styles.highlight : ''}
                                >
                                  Admin
                                </span>
                              </p>
                            </section>
                          </div>
                        </div>
                        <div className={`${styles.field} ${isAdmin ? '' : 'hide'} font-16 font-wt-600`}>
                          <div
                            role="presentation"
                            className={styles.close}
                            onClick={() => {
                              if (isAdmin) {
                                this.disAssociateUser(user);
                              }
                            }}
                          >
                            <img src={close} alt="close" />
                          </div>
                        </div>
                      </div>
                    ))
                  }
                  {projectUsers.length === 0 &&
                    <p className="pad-v-4-4 text-center font-16">No records</p>
                  }
                </Fragment>
              }
              {isAdmin && tabs.audit &&
                <Fragment>
                  <div className={styles.header}>
                    {header.audit.map(label => (
                      <div key={label} className={`${styles.field} font-16 font-wt-600`}>
                        {label}
                      </div>
                      ))
                    }
                    <div className={`${styles.field} ${styles.half} font-16 font-wt-600`}>
                      Description
                    </div>
                  </div>
                  {audits.result && audits.result.map(audit => (
                    <div className={styles.field_row} key={Math.random()}>
                      <div className={`${styles.field} font-14`}>
                        {audit.action_user}
                      </div>
                      <div className={`${styles.field} font-14`}>
                        {moment(audit.time).format('DD-MMM-YYYY HH:mm:ss:SSS')}
                      </div>
                      <div className={`${styles.field} ${styles.half} font-14`} title={audit.action}>
                        {audit.action}
                      </div>
                    </div>
                    ))
                  }
                  {audits.result === 0 &&
                    <p className="pad-v-4-4 text-center font-16">No records</p>
                  }
                </Fragment>
              }
              {isAdmin && tabs.tags &&
                <Fragment>
                  <div className={styles.header}>
                    {header.tags.map(label => (
                      <div key={label} className={`${styles.field} font-16 font-wt-600`}>
                        {label}
                      </div>
                      ))
                    }
                  </div>
                  {tags.result && tags.result.map(tag => (
                    <div className={styles.field_row} key={Math.random()}>
                      <div className={`${styles.field} font-14`}>
                        {tag.name}
                      </div>
                      <div className={`${styles.field} font-14`}>
                        {tag.added_by_name}
                      </div>
                      <div className={`${styles.field} font-14`} title={tag.description}>
                        {tag.description}
                      </div>
                      <div className={`${styles.field} ${styles.last} font-16 font-wt-600`}>
                        <div role="presentation" onClick={() => { this.approveTag(tag, true); }}>
                          <span className={styles.approve}>Approve</span>
                        </div>
                        <div role="presentation" onClick={() => { this.showModal(tag.id); }}>
                          <span className={styles.disapprove}>Disapprove</span>
                        </div>
                      </div>
                    </div>
                    ))
                  }
                  {tags.total_records === 0 &&
                    <p className="pad-v-4-4 text-center font-16">No records</p>
                  }
                </Fragment>
              }
              {isAdmin && tabs.audit &&
                <Paginator
                  params={{
                    page,
                    limit,
                    keyboard: true,
                    goTo: this.goTo,
                    style: {
                      bottom: (limit === 10) ? 0 : '',
                      display: `${audits.result && audits.result.length >= 10 ? 'block' : ''}`,
                      top: `${audits.result && audits.result.length >= 10 ? ((audits.result.length * 50) + 50) : 500}px`
                    },
                    loadData: this.loadData,
                    show: audits.total_records > 0,
                    total: audits.total_records || 0
                  }}
                />
              }
              {isAdmin && tabs.tags &&
                <Paginator
                  params={{
                    page,
                    limit,
                    keyboard: true,
                    goTo: this.goTo,
                    style: {
                      bottom: (limit === 10) ? 0 : '',
                      display: `${tags.result && tags.result.length >= 10 ? 'block' : ''}`,
                      top: `${tags.result && tags.result.length >= 10 ? ((tags.result.length * 50) + 50) : 500}px`
                    },
                    loadData: this.loadData,
                    show: tags.total_records > 0,
                    total: tags.total_records || 0
                  }}
                />
              }
            </div>
          </div>
          <ConfirmOption
            params={{
              showModal,
              proceed: this.approveTag,
              handleHide: this.handleHide
            }}
          />
        </section>
      </div>
    );
  }
}

UserMapping.contextTypes = {
  router: PropTypes.shape({}).isRequired
};

UserMapping.propTypes = {
  notify: PropTypes.func,
  mapUsers: PropTypes.func,
  toggleLoader: PropTypes.func,
  getProjectRoles: PropTypes.func,
  disAssociateUser: PropTypes.func,
  fetchProjectUsers: PropTypes.func,
  fetchCurrentProject: PropTypes.func,
  updateUserPermission: PropTypes.func,
  fetchNonProjectUsers: PropTypes.func,
  project: PropTypes.oneOfType([PropTypes.object]),
  projectRoles: PropTypes.arrayOf(PropTypes.string),
  projectUsers: PropTypes.arrayOf(PropTypes.object),
  nonProjectUsers: PropTypes.arrayOf(PropTypes.object)
};

UserMapping.defaultProps = {
  project: {},
  notify: noFunc,
  projectRoles: [],
  mapUsers: noFunc,
  projectUsers: [],
  nonProjectUsers: [],
  toggleLoader: noFunc,
  getProjectRoles: noFunc,
  disAssociateUser: noFunc,
  fetchProjectUsers: noFunc,
  fetchCurrentProject: noFunc,
  updateUserPermission: noFunc,
  fetchNonProjectUsers: noFunc
};

export default UserMapping;
