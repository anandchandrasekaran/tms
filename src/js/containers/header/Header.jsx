import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
// custom
import styles from './Header.scss';
import { noFunc, formatMessage } from '../../utils/common';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenu: false,
      showNotification: false
    };
  }

  componentDidMount() {
    this.ismounted = true;
    this.props.fetchNotifications();
  }

  componentWillUnmount() {
    this.ismounted = false;
  }

  toggleMenu = () => {
    this.setState({ showMenu: !this.state.showMenu });
  };

  redirect = route => {
    this.context.router.history.push(`/${route}`);
  };

  toggleNotification = () => {
    this.setState({
      showNotification: !this.state.showNotification
    }, () => {
      if (this.state.showNotification) {
        const height = document.getElementById('main-content').offsetHeight;
        document.getElementById('notification').style.maxHeight = `${height}px`;
      }
    });
  };

  closePopup = type => {
    const clearTimer = () => {
      if (this.focusOutTimer) {
        window.clearTimeout(this.focusOutTimer);
      }
    };
    clearTimer();
    this.focusOutTimer = window.setTimeout(() => {
      clearTimer();
      if (this.ismounted) {
        this.setState({[type]: false });
      }
    }, 800);
  };

  getName = user => {
    let name = 'Default';
    if (user.name) {
      name = user.name;
    } else if (user.email) {
      name = user.email;
    }
    return name;
  };

  render() {
    const { user, notifications } = this.props;
    const { showMenu, showNotification } = this.state;
    return (
      <div className={styles.header}>
        <div className={styles.container}>
          <div
            role="presentation"
            className={styles.logo}
            onClick={() => { this.redirect('home'); }}
          >
            <img src="https://madstreetden.com/static/images/msd-logo.svg" alt="madstreetden" />
          </div>
          <div className={styles.rhs}>
            <div className={styles.products}>
              <i className="fa fa-ellipsis-h" aria-hidden="true" />
              <i className="fa fa-ellipsis-h" aria-hidden="true" />
              <i className="fa fa-ellipsis-h" aria-hidden="true" />
            </div>
            <div
              role="presentation"
              className={styles.notify}
              onClick={this.toggleNotification}
            >
              <p
                tabIndex="1"
                className={styles.icon}
                onBlur={() => { this.closePopup('showNotification'); }}
              >
                <i className="fa fa-bell-o" aria-hidden="true" />
                <sup className={notifications.count > 0 ? '' : 'hide'}>{notifications.count}</sup>
              </p>
              {showNotification && notifications.notifications &&
                <div className={`${styles.notifications} slim-scroll fadeInDown`} id="notification">
                  <div className={styles.title}>Notifications</div>
                  {notifications.notifications && (notifications.notifications.length > 0) &&
                    notifications.notifications.map(notification => (
                    <div
                      role="presentation"
                      key={notification.id}
                      onClick={() => {
                        if (!notification.read) {
                          this.props.markAsRead(notification.id);
                        }
                      }}
                      className={`${styles.field} ${notification.read ? '' : 'font-wt-600'}`}
                    >
                      <p>{notification.message}</p>
                      <p className="txt-12 color-2 font-wt-400">
                        {moment(notification.created_date).fromNow()}
                      </p>
                    </div>
                    ))
                  }
                  {(notifications.total_count === 0) &&
                    <p className="text-center txt-16 pad-10">No message</p>
                  }
                  {(notifications.total_count > 10) &&
                    <div
                      role="presentation"
                      className={styles.link}
                      onClick={() => {
                        this.redirect('notifications');
                      }}
                    >
                      <p className="cur-pointer">
                        See all recent activity
                      </p>
                    </div>
                  }
                </div>
              }
            </div>
            <div className={styles.profile}>
              <section
                tabIndex="2"
                role="presentation"
                onClick={this.toggleMenu}
                onBlur={() => { this.closePopup('showMenu'); }}
              >
                <ul>
                  <li className={styles.cname}>
                    <span className={styles.name}>
                      {this.getName(user)}
                    </span>
                    <span className={styles.down} />
                    {showMenu &&
                      <ul className={styles.dropdown}>
                        {user.isAdmin &&
                          <li role="presentation" onClick={() => { this.redirect('user'); }}>
                            <i className="fa fa-cog" aria-hidden="true" />
                            &nbsp;Settings
                          </li>
                        }
                        <li role="presentation" onClick={this.props.logout}>
                          <i className="fa fa-sign-out" aria-hidden="true" />
                          &nbsp;Logout
                        </li>
                      </ul>
                    }
                  </li>
                </ul>
              </section>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Header.contextTypes = {
  router: PropTypes.shape({}).isRequired
};

Header.propTypes = {
  logout: PropTypes.func,
  user: PropTypes.oneOfType([
    PropTypes.object
  ]),
  markAsRead: PropTypes.func,
  fetchNotifications: PropTypes.func,
  notifications: PropTypes.oneOfType([ PropTypes.object ])
};

Header.defaultProps = {
  user: {},
  logout: noFunc,
  notifications: {},
  markAsRead: noFunc,
  fetchNotifications: noFunc
};

export default Header;
