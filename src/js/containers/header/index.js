import { connect } from 'react-redux';
import Header from './Header';

const mapStateToProps = state => ({
  user: state.user.info,
  notifications: state.notifications.list
});

const mapDispatchToProps = dispatch => ({
  logout: () => {
    dispatch({ type: 'USER:LOGOUT' });
  },
  markAsRead: id => {
    dispatch({ type: 'NOTIFICATION:READ', id });
  },
  fetchNotifications: () => {
    dispatch({ type: 'NOTIFICATIONS:FETCH' });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
