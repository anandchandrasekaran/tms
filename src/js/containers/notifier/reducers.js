import Immutable from 'seamless-immutable';

const defaultState = Immutable.flatMap({
  showMessage: false,
  notification: { type: '', message: '' }
});

function mergeData(state, data) {
  return Immutable.merge(state, data);
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'NOTIFIER:SHOW':
      return mergeData(state, {
        showMessage: true,
        notification: action.notification
      });
    case 'NOTIFIER:HIDE':
      return mergeData(state, {
        showMessage: false,
        notification: { type: '', message: '' }
      });
    default:
      return state;
  }
};
