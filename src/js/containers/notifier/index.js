import { connect } from 'react-redux';

import Notifier from './notifier';

const mapStateToProps = state => ({
  showMessage: state.notifier.showMessage,
  notification: state.notifier.notification
});

const mapDispatchToProps = dispatch => ({
  hideNotifier: () => {
    dispatch({ type: 'NOTIFIER:HIDE' });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Notifier);
