import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
// custom css
import styles from './styles.scss';

/**
 * This class describes a notifier.
 *
 * @class      Notifier (name)
 */
class Notifier extends Component {
  componentDidUpdate() {
    if (this.props.showMessage) {
      const timer = window.setTimeout(() => {
        window.clearTimeout(timer);
        this.props.hideNotifier();
      }, 2000);
    }
  }

  render() {
    const { showMessage, notification } = this.props;
    return (
      <Fragment>
        {showMessage &&
          <div
            key={Math.random()}
            style={{ top: '95px' }}
            className={`${styles.notifier} ${styles[notification.type]}`}
          >
            <img
              alt="notification"
              className={styles.img}
              src={`https://static-dev.vuetag.ai/assets/admin/images/${notification.type}.svg`}
            />
            <div className={styles.content}>
              <div className={styles.text}>{notification.type}</div>
              <div className="notifier-sub-text">{notification.message}</div>
            </div>
          </div>
        }
      </Fragment>
    );
  }
}

Notifier.propTypes = {
  showMessage: PropTypes.bool.isRequired,
  hideNotifier: PropTypes.func.isRequired,
  notification: PropTypes.oneOfType([PropTypes.object]).isRequired
};
export default Notifier;
