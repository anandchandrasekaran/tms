import PropTypes from 'prop-types';
import React, { Component } from 'react';
// custom modules and components
import {
  noFunc,
  isEmpty,
  validateEmail
} from '../../utils/common';
import styles from './style.scss';
import Banner from '../../components/Banner';

// Images
const keyValue = {
  name: 'Name',
  email: 'Email',
  password: 'Password',
  username: 'User name',
  org_name: 'Organization',
  org_city: 'Organization city',
  org_country: 'Organization country'
};
const mail = 'https://static-dev.vuetag.ai/assets/images/Mail.svg';
const name = 'https://static-dev.vuetag.ai/assets/images/Name.svg';
const userName = 'https://static-dev.vuetag.ai/assets/images/User_new.svg';
const organization = 'https://static.vuetag.ai/assets/images/Company_new.svg';
const passwordIcon = 'https://static-dev.vuetag.ai/assets/images/Password.svg';
const fields = ['name', 'username', 'email', 'password', 'org_name', 'org_city', 'org_country'];

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      formData: {}
    };
  }

  trimFieldValue = evt => {
    const { formData } = this.state;
    const { id, value } = evt.target;
    formData[id] = isEmpty(value) ? '' : value.trim();
    this.setState({ formData });
    this.validateField(id, value);
  };

  /**
   * validate
   * Validates the field value on focus out
   */
  validate = evt => {
    const { formData } = this.state;
    const { id, value } = evt.target;
    formData[id] = value;
    this.setState({ formData });
    this.validateField(id, value);
  };

  validateField = (key, value) => {
    let { errors } = this.state;
    if (isEmpty(value)) {
      this.isValid = false;
      errors[key] = `${keyValue[key]} cannot be empty`;
    } else if (key === 'email' && validateEmail(value)) {
      this.isValid = false;
      errors[key] = 'Email format is not valid';
    } else {
      delete errors[key];
    }
    this.setState({ errors });
  };

  /**
   * validatForm
   *
   * Validates all the fields value
   */
  validatForm = formData => {
    fields.forEach(field => {
      this.validateField(field, formData[field]);
    });
  };

  handleSubmit = evt => {
    evt.preventDefault();
    const { formData } = this.state;
    this.isValid = true;
    this.validatForm(formData);
    if (this.isValid) {
      this.props.signup(formData);
    }
  };

  redirect = evt => {
    evt.preventDefault();
    this.context.router.history.push('/login');
  };

  render() {
    const { errors, formData } = this.state;
    return (
      <div className={styles.signup}>
        <Banner />
        <div id="signupForm" className={styles.container}>
          <div className={styles.inner}>
            <form className={styles.form} onSubmit={this.handleSubmit}>
              <p>MSD Taxonomy</p>
              <p className="font-xlarge fw-500 color-2">
                Sign Up
              </p>
              <div className="inp-elm text-box grid-col-16 al-l-m font-small">
                <div className="font-medium p-l-r-10">
                  <img alt="name" src={name} />
                  <input
                    id="name"
                    type="text"
                    placeholder="Name"
                    onChange={this.validate}
                    value={formData.name || ''}
                    onBlur={this.trimFieldValue}
                  />
                </div>
                {errors && errors.name &&
                  <span className="color-6">{errors.name}</span>
                }
              </div>
              <div className="inp-elm text-box grid-col-16 al-l-m font-small">
                <div className="font-medium p-l-r-10">
                  <img alt="username" src={userName} />
                  <input
                    type="text"
                    id="username"
                    placeholder="User name"
                    onChange={this.validate}
                    onBlur={this.trimFieldValue}
                    value={formData.username || ''}
                  />
                </div>
                {errors && errors.username &&
                  <span className="color-6">{errors.username}</span>
                }
              </div>
              <div className="inp-elm text-box grid-col-16 al-l-m font-small">
                <div className="font-medium p-l-r-10">
                  <img alt="mail" src={mail} />
                  <input
                    id="email"
                    type="text"
                    placeholder="E-mail"
                    onChange={this.validate}
                    autoComplete="new-email"
                    onBlur={this.trimFieldValue}
                    value={formData.email || ''}
                  />
                </div>
                {errors && errors.email &&
                  <span className="color-6">{errors.email}</span>
                }
              </div>
              <div className="inp-elm pass-box grid-col-16 al-l-m font-small">
                <div className="font-medium p-l-r-10">
                  <img alt="password" src={passwordIcon} />
                  <input
                    id="password"
                    type="password"
                    placeholder="Password"
                    onChange={this.validate}
                    autoComplete="new-password"
                    onBlur={this.trimFieldValue}
                    value={formData.password || ''}
                  />
                </div>
                {errors && errors.password &&
                  <span className="color-6">{errors.password}</span>
                }
              </div>
              <div className="inp-elm text-box grid-col-16 al-l-m font-small">
                <div className="font-medium p-l-r-10">
                  <img alt="organization" src={organization} />
                  <input
                    type="text"
                    id="org_name"
                    onChange={this.validate}
                    autoComplete="organization"
                    placeholder="Organization"
                    onBlur={this.trimFieldValue}
                    value={formData.org_name || ''}
                  />
                </div>
                {errors && errors.org_name &&
                  <span className="color-6">{errors.org_name}</span>
                }
              </div>
              <div className="inp-elm text-box grid-col-16 al-l-m font-small">
                <div className="font-medium p-l-r-10">
                  <img alt="org_city" src={organization} />
                  <input
                    type="text"
                    id="org_city"
                    autoComplete="org_city"
                    onChange={this.validate}
                    onBlur={this.trimFieldValue}
                    placeholder="Organization city"
                    value={formData.org_city || ''}
                  />
                </div>
                {errors && errors.org_city &&
                  <span className="color-6">{errors.org_city}</span>
                }
              </div>
              <div className="inp-elm text-box grid-col-16 al-l-m font-small">
                <div className="font-medium p-l-r-10">
                  <img alt="org_country" src={organization} />
                  <input
                    type="text"
                    id="org_country"
                    onChange={this.validate}
                    autoComplete="org_country"
                    onBlur={this.trimFieldValue}
                    placeholder="Organization country"
                    value={formData.org_country || ''}
                  />
                </div>
                {errors && errors.org_country &&
                  <span className="color-6">{errors.org_country}</span>
                }
              </div>
              <div className={`${styles.submit} grid-col-16 al-c-m`}>
                <button type="submit" className="btn btn-gradient grid-col-16 pad-10-px font-medium">
                  Sign Up
                </button>
              </div>
              <div className={`${styles.submit} grid-col-16 al-c-m`}>
                <button
                  type="button"
                  onClick={this.redirect}
                  className="btn btn-normal grid-col-16 pad-10-px font-medium"
                >
                  Sign In
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

Signup.contextTypes = {
  router: PropTypes.shape({}).isRequired
};

Signup.propTypes = {
  signup: PropTypes.func
};

Signup.defaultProps = {
  signup: noFunc
};

export default Signup;
