import { connect } from 'react-redux';
import Signup from './signup';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  signup: data => {
    dispatch({ type: 'USER:SIGN_UP', data });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
