import Immutable from 'seamless-immutable';

const defaultState = Immutable.flatMap({
  list: {},
  markAsRead: null
});

function mergeData(state, data) {
  return Immutable.merge(state, data);
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'NOTIFICATIONS:FETCH:FAILED':
    case 'NOTIFICATIONS:FETCH:SUCCESS':
      return mergeData(state, { list: action.notifications });
    case 'NOTIFICATION:READ:FAILED':
    case 'NOTIFICATION:READ:SUCCESS':
      return mergeData(state, { markAsRead: action.markAsRead });
    default:
      return state;
  }
};
