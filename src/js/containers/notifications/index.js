import { connect } from 'react-redux';
import Notifications from './notifications';

const mapStateToProps = state => ({
  notifications: state.notifications.list
});

const mapDispatchToProps = dispatch => ({
  markAsRead: id => {
    dispatch({ type: 'NOTIFICATION:READ', id });
  },
  fetchNotifications: params => {
    dispatch({ type: 'NOTIFICATIONS:FETCH', params });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);
