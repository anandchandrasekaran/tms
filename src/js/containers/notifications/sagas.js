import { put } from 'redux-saga/effects';

import { request, formatMessage } from '../../utils/common';

/**
 * { generator_description }
 *
 * @param      {<type>}  action  The action
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* fetch({ params }) {
  yield put({ type: 'LOADER:SHOW' });

  if (!params) {
    params = {};
  }

  const page = params.page || 1;
  const limit = params.limit || 10;
  const { response, isSuccess } = yield request().get(`/v1/notifications/?page_no=${page}&limit=${limit}`);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({ type: 'NOTIFICATIONS:FETCH:SUCCESS', notifications: response });
  } else {
    yield put({ type: 'NOTIFICATIONS:FETCH:FAILED', notifications: {} });
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in fetching notifications'
      }
    });
  }
}

/**
 * { generator_description }
 *
 * @param      {<type>}  action  The action
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* markAsRead({ id }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().post('/v1/notifications/', { ids: [id] });

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({ type: 'NOTIFICATION:READ:SUCCESS', markAsRead: true });
    // On success of mark as read, update the list
    yield put({ type: 'NOTIFICATIONS:FETCH' });
  } else {
    yield put({ type: 'NOTIFICATION:READ:FAILED', markAsRead: false });
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in marking notification as read'
      }
    });
  }
}
