import moment from 'moment';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
// custom
import styles from './style.scss';
import { noFunc } from '../../utils/common';
import Request from '../../services/request';
import Paginator from '../../components/Paginator';
import Breadcrumb from '../../components/Breadcrumb';

const headerFields = ['Id', 'Message', 'Created Time', 'Mark as read'];
class Notifications extends Component {
  constructor(props) {
    super(props);
    this.sideMenu = [{
      name: 'default',
      link: '',
      icon: ''
    }, {
      name: 'Projects',
      link: '/project',
      icon: 'home'
    }, {
      name: 'Users',
      link: '/user',
      icon: 'users'
    }, {
      name: 'Notifications',
      link: '/notifications',
      icon: 'envelope'
    }];
    this.state = {
      page: 1,
      limit: 10
    };
  }

  componentDidMount() {
    this.fetchNotifications();
  }

  fetchNotifications = () => {
    const { page, limit } = this.state;
    this.props.fetchNotifications({ page, limit });
  };

  /**
   * `goTo`
   * It is used in pagination. When the user clicks on each pagination cell, it will make request to fetch images
   */
  goTo = page => {
    if (this.state.page !== page) {
      this.setState({
        page
      }, () => {
        this.fetchNotifications();
      });
    }
  };

  loadData = () => {
    const limit = document.getElementById('pagelimit').value;
    this.setState({
      limit,
      page: 1
    }, () => {
      this.fetchNotifications();
    });
  };

  redirect = link => {
    this.context.router.history.push(link);
  };

  render() {
    const { page, limit } = this.state;
    const { notifications } = this.props;
    const totalRecords = notifications.total_count;
    return (
      <div className={styles.notifications}>
        <aside className={styles.nsidemenu}>
          {this.sideMenu.map(menu => (
              <div className={styles.link} key={menu.name}>
                {menu.link &&
                  <span
                    title={menu.name}
                    role="presentation"
                    onClick={() => { this.redirect(menu.link); }}
                  >
                    <i className={`fa fa-${menu.icon}`} aria-hidden="true" />
                  </span>
                }
              </div>
            ))
          }
        </aside>
        <section id="main-content" className={styles.container}>
          <div className={styles.breadcrumb}>
            <div className={styles.title}>
              <Breadcrumb
                params={{
                  push: this.context.router.history.push,
                  list: [{
                    link: '/project',
                    name: 'Projects'
                  }, {
                    link: '',
                    name: 'Notifications'
                  }]
                }}
              />
            </div>
          </div>
          <div className={`${styles.content} slim-scroll`}>
            {(totalRecords > 0) &&
              <div className={styles.row}>
                <div className={styles.header}>
                  {headerFields.map(field => (
                      <div key={field} className={styles.field}>
                        {field}
                      </div>
                    ))
                  }
                </div>
                {notifications.notifications &&
                  notifications.notifications.map(notification => (
                    <div
                      key={`${Math.random()}-${notification.id}`}
                      className={`${styles.field_row} ${notification.read ? '' : 'font-wt-600'}`}
                    >
                      <div className={`${styles.field} font-16`}>
                        {notification.id}
                      </div>
                      <div className={`${styles.field} font-16`} title={notification.message}>
                        {notification.message}
                      </div>
                      <div className={`${styles.field} font-16`}>
                        {moment(notification.created_date).format('DD-MMM-YYYY HH:mm:ss')}
                      </div>
                      <div className={`${styles.field} ${notification.read ? styles.disabled : ''}`}>
                        <span
                          role="presentation"
                          onClick={() => {
                            if (!notification.read) {
                              this.props.markAsRead(notification.id);
                            }
                          }}
                        >
                          <i className={`fa fa-envelope-${notification.read ? 'open-o' : 'o'}`} aria-hidden="true" />
                        </span>
                      </div>
                    </div>
                  ))
                }
              </div>
            }
            {(totalRecords === 0) &&
              <div className={styles.empty}>
                <p className="theme-color font-wt-600 font-18">
                  No notifications
                </p>
              </div>
            }
            <Paginator
              params={{
                page,
                limit,
                keyboard: true,
                goTo: this.goTo,
                show: totalRecords > 0,
                loadData: this.loadData,
                total: totalRecords || 0
              }}
            />
          </div>
        </section>
      </div>
    );
  }
}

Notifications.contextTypes = {
  router: PropTypes.shape({}).isRequired
};

Notifications.propTypes = {
  markAsRead: PropTypes.func,
  fetchNotifications: PropTypes.func,
  notifications: PropTypes.oneOfType([PropTypes.object])
};

Notifications.defaultProps = {
  notifications: {},
  markAsRead: noFunc,
  fetchNotifications: noFunc
};

export default Notifications;
