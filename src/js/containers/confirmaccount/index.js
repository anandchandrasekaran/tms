import { connect } from 'react-redux';
import ConfirmAccount from './confirmAccount';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  verifyUser: (userId, data) => {
    dispatch({ type: 'VERIFY:USER', userId, data });
  },
  notify: notification => {
    dispatch({ type: 'NOTIFIER:SHOW', notification });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmAccount);
