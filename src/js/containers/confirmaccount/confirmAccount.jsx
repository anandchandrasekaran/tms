import React, { Component } from 'react';
import PropTypes from 'prop-types';
// custom modules and components
import styles from './style.scss';
import Banner from '../../components/Banner';
import { noFunc, isEmpty } from '../../utils/common';
import { extractQueryParams } from '../../utils/routerHooks';

// Images
const keyValue = {
  name: 'Name',
  password: 'Password',
  username: 'User name'
};
const fields = ['name', 'username', 'password'];
// static assets
const name = 'https://static-dev.vuetag.ai/assets/images/Name.svg';
const userName = 'https://static-dev.vuetag.ai/assets/images/User_new.svg';
const passwordIcon = 'https://static-dev.vuetag.ai/assets/images/Password.svg';

/**
 * This class describes a confirm account.
 *
 * @class      ConfirmAccount (name)
 */
class ConfirmAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      formData: {}
    };
  }

  /**
   * validate
   * Validates the field value on focus out
   */
  validate = evt => {
    const { formData } = this.state;
    const { id, value } = evt.target;
    formData[id] = isEmpty(value) ? '' : value.trim();
    this.setState({ formData });
    this.validateField(id, value);
  };

  validateField = (key, value) => {
    let { errors } = this.state;
    if (isEmpty(value)) {
      this.isValid = false;
      errors[key] = `${keyValue[key]} cannot be empty`;
    } else {
      delete errors[key];
    }
    this.setState({ errors });
  };

  /**
   * validatForm
   *
   * Validates all the fields value
   */
  validatForm = formData => {
    fields.forEach(field => {
      this.validateField(field, formData[field]);
    });
  };

  handleSubmit = evt => {
    evt.preventDefault();
    const { formData } = this.state;
    this.isValid = true;
    this.validatForm(formData);
    if (this.isValid) {
      const { activation_token, user_id } = extractQueryParams();
      if (isEmpty(user_id)) {
        this.props.notify({
          type: 'error',
          message: 'User id is mandatory in the activation link'
        });
      } else if (isEmpty(activation_token)) {
        this.props.notify({
          type: 'error',
          message: 'Activation token is mandatory in the activation link'
        });
      } else {
        formData.activation_token = activation_token;
        this.props.verifyUser(user_id, formData);
      }
    }
  };

  render() {
    const { errors, formData } = this.state;
    return (
      <div className={styles.root}>
        <Banner />
        <div id="confirmaccount" className={styles.rhs}>
          <div className={styles.inner}>
            <form className={styles.form} onSubmit={this.handleSubmit}>
              <p>MSD Taxonomy</p>
              <p className="font-xlarge fw-500 color-2">
                Set Password
              </p>
              <div className="inp-elm text-box grid-col-16 al-l-m font-small">
                <div className="font-medium p-l-r-10">
                  <img alt="name" src={name} />
                  <input
                    id="name"
                    type="text"
                    placeholder="Name"
                    onBlur={this.validate}
                    onChange={this.validate}
                    value={formData.name || ''}
                  />
                </div>
                {errors && errors.name &&
                  <span className="color-6">{errors.name}</span>
                }
              </div>
              <div className="inp-elm text-box grid-col-16 al-l-m font-small">
                <div className="font-medium p-l-r-10">
                  <img alt="username" src={userName} />
                  <input
                    type="text"
                    id="username"
                    onBlur={this.validate}
                    placeholder="User name"
                    onChange={this.validate}
                    value={formData.username || ''}
                  />
                </div>
                {errors && errors.username &&
                  <span className="color-6">{errors.username}</span>
                }
              </div>
              <div className="inp-elm pass-box grid-col-16 al-l-m font-small">
                <div className="font-medium p-l-r-10">
                  <img alt="password" src={passwordIcon} />
                  <input
                    id="password"
                    type="password"
                    autoComplete="new-password"
                    placeholder="Password"
                    onBlur={this.validate}
                    onChange={this.validate}
                    value={formData.password || ''}
                  />
                </div>
                {errors && errors.password &&
                  <span className="color-6">{errors.password}</span>
                }
              </div>
              <div className={`${styles.submit} grid-col-16 al-c-m`}>
                <button type="submit" className="btn btn-gradient grid-col-16 pad-10-px font-medium">
                  Submit
                </button>
              </div>
              <div className={`${styles.submit} grid-col-16 al-c-m`}>
                <button
                  type="button"
                  onClick={evt => {
                    evt.preventDefault();
                    this.context.router.history.push('/signin');
                  }}
                  className="btn btn-normal grid-col-16 pad-10-px font-medium"
                >
                  Already have account?
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

ConfirmAccount.contextTypes = {
  router: PropTypes.shape({}).isRequired
};

ConfirmAccount.propTypes = {
  notify: PropTypes.func,
  verifyUser: PropTypes.func
};

ConfirmAccount.defaultProps = {
  notify: noFunc,
  verifyUser: noFunc
};

export default ConfirmAccount;
