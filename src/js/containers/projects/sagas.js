import { put } from 'redux-saga/effects';

import { request, formatMessage } from '../../utils/common';

/**
 * createProject
 *
 * @param      {Object}  arg1       The argument 1
 * @param      {<type>}  arg1.data  The data
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* createProject({ data }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().post('/v1/projects/', data);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'success',
        message: 'New project created'
      }
    });
    yield put({ type: 'PROJECT:CREATE:SUCCESS', project: response });
  } else {
    yield put({ type: 'PROJECT:CREATE:FAILED', project: {} });
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in creating project'
      }
    });
  }
}

/**
 * get
 *
 * @param      {Object}  arg1            The argument 1
 * @param      {<type>}  arg1.projectId  The project identifier
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* get({ projectId }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().get(`/v1/projects/${projectId}/`);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({ type: 'PROJECT:FETCH:SUCCESS', project: response || {} });
  } else {
    yield put({ type: 'PROJECT:FETCH:FAILED', project: {} });
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in fetching project detail'
      }
    });
  }
}

/**
 * fetchProjects
 *
 * @param      {Object}  arg1       The argument 1
 * @param      {<type>}  arg1.data  The data
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* fetchProjects({ params }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().get(`/v1/projects/?limit=${params.limit}&page_no=${params.page}`);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    response.list = JSON.parse(JSON.stringify(response.result));
    delete response.result;
    yield put({ type: 'PROJECT_LIST:FETCH:SUCCESS', projects: response || {} });
  } else {
    yield put({ type: 'PROJECT_LIST:FETCH:FAILED', projects: {} });
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in fetching projects'
      }
    });
  }
}

/**
 * fetchUsers
 *
 * @param      {Object}  arg1            The argument 1
 * @param      {<type>}  arg1.projectId  The project identifier
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* fetchUsers({ projectId }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().get(`/v1/projects/${projectId}/users/?is_proj_users=true`);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({ type: 'PROJECT_USERS:FETCH:SUCCESS', list: response.result, keyName: 'projectUsers' });
  } else {
    yield put({ type: 'PROJECT_USERS:FETCH:FAILED', list: [], keyName: 'projectUsers' });
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in fetching project users'
      }
    });
  }
}

/**
 * fetchNonUsers
 *
 * @param      {Object}  arg1            The argument 1
 * @param      {<type>}  arg1.projectId  The project identifier
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* fetchNonUsers({ projectId }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().get(`/v1/projects/${projectId}/users/?is_proj_users=false`);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    const formatted = [];
    const list = response.result;
    if (list.length > 0) {
      list.forEach(iteratee => {
        const obj = iteratee;
        const { role } = iteratee;
        obj.user.role = role;
        formatted.push(obj.user);
      });
    }
    yield put({ type: 'PROJECT_USERS:FETCH:SUCCESS', list: formatted, keyName: 'nonProjectUsers' });
  } else {
    yield put({ type: 'PROJECT_USERS:FETCH:FAILED', list: [], keyName: 'nonProjectUsers' });
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in fetching project users'
      }
    });
  }
}

/**
 * fetchRoles
 *
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* fetchRoles() {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().get('/v1/projects/roles/');

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({ type: 'PROJECT_ROLES:FETCH:SUCCESS', data: response.result || [] });
  } else {
    yield put({ type: 'PROJECT_ROLES:FETCH:FAILED', data: [] });
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in fetching project roles'
      }
    });
  }
}

/**
 * updateProjectPermission
 *
 * @param      {Object}  arg1            The argument 1
 * @param      {<type>}  arg1.projectId  The project identifier
 * @param      {<type>}  arg1.data       The data
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* updateProjectPermission({ projectId, data }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().patch(`/v1/projects/${projectId}/users/`, data);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'success',
        message: response.message
      }
    });
    yield put({ type: 'PROJECT_PERMISSION:UPDATE:SUCCESS', data: response.result || [] });
    yield put({ type: 'PROJECT_USERS:FETCH', projectId });
  } else {
    yield put({ type: 'PROJECT_PERMISSION:UPDATE:FAILED', data: [] });
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in changing permission'
      }
    });
  }
}

/**
 * disAssociateUser
 *
 * @param      {Object}  arg1            The argument 1
 * @param      {<type>}  arg1.projectId  The project identifier
 * @param      {<type>}  arg1.userId     The user identifier
 * @return     {<type>}  { description_of_the_return_value }
 */
export function* disAssociateUser({ projectId, userId }) {
  yield put({ type: 'LOADER:SHOW' });

  const { response, isSuccess } = yield request().delete(`/v1/projects/${projectId}/user/${userId}/`);

  yield put({ type: 'LOADER:HIDE' });

  if (isSuccess) {
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'success',
        message: response.message
      }
    });
    yield put({ type: 'DISASSOCIATE:USER:SUCCESS', users: response.result || {} });
    yield put({ type: 'PROJECT_USERS:FETCH', projectId });
    yield put({ type: 'PROJECT_NON_USERS:FETCH', projectId });
  } else {
    yield put({ type: 'DISASSOCIATE:USER:FAILED', users: {} });
    yield put({
      type: 'NOTIFIER:SHOW',
      notification: {
        type: 'error',
        message: formatMessage(response) || 'Error in disassociating user from project'
      }
    });
  }
}
