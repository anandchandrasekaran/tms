import Immutable from 'seamless-immutable';

const defaultState = Immutable.flatMap({
  roles: [],
  project: {},
  projects: {},
  projectUsers: [],
  closeModal: false,
  nonProjectUsers: []
});

function mergeData(state, data) {
  return Immutable.merge(state, data);
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'PROJECT:CREATE:FAILED':
    case 'PROJECT:CREATE:SUCCESS':
      return mergeData(state, { closeModal: true });

    case 'PROJECT:FETCH:FAILED':
    case 'PROJECT:FETCH:SUCCESS':
      return mergeData(state, { project: action.project });

    case 'PROJECT_LIST:FETCH:FAILED':
    case 'PROJECT_LIST:FETCH:SUCCESS':
      return mergeData(state, { projects: action.projects || {} });

    case 'PROJECT_ROLES:FETCH:FAILED':
    case 'PROJECT_ROLES:FETCH:SUCCESS':
      return mergeData(state, { roles: action.data || [] });

    case 'PROJECT_USERS:FETCH:FAILED':
    case 'PROJECT_USERS:FETCH:SUCCESS':
      return mergeData(state, { [action.keyName]: action.list || [] });

    case 'PROJECT:MODAL:RESET':
      return mergeData(state, { closeModal: false });

    case 'PROJECTS:RESET':
      return mergeData(state, { projects: {}, closeModal: false });

    default:
      return state;
  }
};
