import { connect } from 'react-redux';
import Projects from './projects';

const mapStateToProps = state => ({
  projects: state.projects.projects,
  closeModal: state.projects.closeModal
});

const mapDispatchToProps = dispatch => ({
  reset: () => {
    dispatch({ type: 'PROJECTS:RESET' });
  },
  loadProjects: params => {
    dispatch({ type: 'PROJECT_LIST:FETCH', params });
  },
  resetModalState: () => {
    dispatch({ type: 'PROJECT:MODAL:RESET' });
  },
  createProject: data => {
    dispatch({ type: 'PROJECT:CREATE', data });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Projects);
