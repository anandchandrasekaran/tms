import moment from 'moment';
import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';
// custom modules and components
import styles from './style.scss';
import { noFunc } from '../../utils/common';
import GridHeader from './components/gridHeader';
import Paginator from '../../components/Paginator';
import Breadcrumb from '../../components/Breadcrumb';
import CreateProject from './components/createProject';

/**
 * This class describes projects.
 *
 * @class      Projects (name)
 */
class Projects extends Component {
  constructor(props) {
    super(props);
    this.sideMenu = [{
      name: 'default',
      link: '',
      icon: ''
    }, {
      name: 'Projects',
      link: '/project',
      icon: 'home'
    }, {
      name: 'Users',
      link: '/user',
      icon: 'users'
    }, {
      name: 'Notifications',
      link: '/notifications',
      icon: 'envelope'
    }];
    this.state = {
      page: 1,
      limit: 10,
      showModal: false
    };
  }

  componentDidMount() {
    this.fetchProjects();
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.closeModal && this.props.closeModal) {
      this.handleHide(true);
    }
  }

  componentWillUnmount() {
    this.props.reset();
  }

  fetchProjects = () => {
    const { page, limit } = this.state;
    this.props.loadProjects({ page, limit });
  };

  /**
   * handleHide
   *
   * @return     {<type>}  Manages to hide and reset the state values on modal close
   */
  handleHide = fetchData => {
    this.setState({
      showModal: false
    }, () => {
      if (fetchData) {
        this.props.resetModalState();
        this.fetchProjects();
      }
    });
  };

  /**
   * Shows the modal.
   *
   * @return     {<type>}  { description_of_the_return_value }
   */
  showModal = () => {
    this.setState({ showModal: true });
  };

  createProject = data => {
    this.props.createProject(data);
  };

  /**
   * `goTo`
   * It is used in pagination. When the user clicks on each pagination cell, it will make request to fetch images
   */
  goTo = page => {
    if (this.state.page !== page) {
      this.setState({
        page
      }, () => {
        this.fetchProjects();
      });
    }
  };

  loadData = () => {
    const limit = document.getElementById('pagelimit').value;
    this.setState({
      limit,
      page: 1
    }, () => {
      this.fetchProjects();
    });
  };

  redirect = link => {
    this.context.router.history.push(link);
  };

  render() {
    const { projects } = this.props;
    const { page, limit, showModal } = this.state;
    return (
      <div className={styles.projects}>
        <aside className={styles.nsidemenu}>
          {this.sideMenu.map(menu => (
              <div className={styles.link} key={menu.name}>
                {menu.link &&
                  <span
                    title={menu.name}
                    role="presentation"
                    onClick={() => { this.redirect(menu.link); }}
                  >
                    <i className={`fa fa-${menu.icon}`} aria-hidden="true" />
                  </span>
                }
              </div>
            ))
          }
        </aside>
        <section id="main-content" className={styles.mcontent}>
          <div className={styles.breadcrumb}>
            <div className={styles.title}>
              <Breadcrumb
                params={{
                  push: this.context.router.history.push,
                  list: [{
                    link: '/project',
                    name: 'Projects'
                  }]
                }}
              />
            </div>
            <div className={styles.actions}>
              <div className={styles.rhs}>
                <div
                  role="presentation"
                  title="Add new project"
                  className={styles.add}
                  onClick={this.showModal}
                >
                  <i className="fa fa-plus" aria-hidden="true" />
                </div>
              </div>
            </div>
          </div>
          <div className={`${styles.content} slim-scroll`}>
            {(projects.total_records > 0) &&
              <Fragment>
                <div className={styles.row}>
                  <div className={styles.header}>
                    {/* TODO: Enable sort option in the header component */}
                    <GridHeader styles={styles.field} />
                  </div>
                  {projects.list &&
                    projects.list.map(project => (
                      <div
                        role="presentation"
                        className={styles.field_row}
                        key={`${Math.random()}-${project.created_date}`}
                        onClick={() => {
                          this.context.router.history.push(`/project/${project.id}`);
                        }}
                      >
                        <div className={`${styles.field} font-16`}>
                          {project.id}
                        </div>
                        <div className={`${styles.field} font-16`}>
                          {project.name}
                        </div>
                        <div className={`${styles.field} font-16`}>
                          {project.description}
                        </div>
                        <div className={styles.field}>
                          <div>
                            <section>
                              <p className="font-16 color-1" title={project.created_date}>
                                {moment(project.created_date).format('DD MMM YYYY HH:mm:ss')}
                              </p>
                            </section>
                          </div>
                        </div>
                        <div className={`${styles.field} font-16`}>
                          {moment(project.modified_date).format('DD MMM YYYY HH:mm:ss')}
                        </div>
                      </div>
                    ))
                  }
                </div>
                <Paginator
                  params={{
                    page,
                    limit,
                    keyboard: true,
                    goTo: this.goTo,
                    loadData: this.loadData,
                    show: projects.total_records > 0,
                    total: projects.total_records || 0
                  }}
                />
              </Fragment>
            }
            {(projects.total_records === 0) &&
              <div className={styles.empty}>
                <p className="theme-color font-wt-600 font-18">
                  No project created
                </p>
              </div>
            }
          </div>
          <CreateProject
            params={{
              showModal,
              create: this.createProject,
              handleHide: this.handleHide
            }}
          />
        </section>
      </div>
    );
  }
}

Projects.contextTypes = {
  router: PropTypes.shape({}).isRequired
};

Projects.propTypes = {
  reset: PropTypes.func,
  closeModal: PropTypes.bool,
  loadProjects: PropTypes.func,
  createProject: PropTypes.func,
  resetModalState: PropTypes.func,
  projects: PropTypes.oneOfType([PropTypes.object])
};

Projects.defaultProps = {
  projects: {},
  reset: noFunc,
  closeModal: false,
  loadProjects: noFunc,
  createProject: noFunc,
  resetModalState: noFunc
};

export default Projects;
