import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, ModalHeader, ModalBody } from 'msd-modal';
// custom components
import styles from './style.scss';
import { isEmpty } from '../../../../utils/common';

const keyValue = {
  name: 'Project name',
  description: 'Project description'
};
const fields = ['name', 'description'];
const close = 'https://static-dev.vuetag.ai/assets/images/close.svg';

class CreateProject extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      formData: {}
    };
  }

  trimFieldValue = evt => {
    const { formData } = this.state;
    const { id, value } = evt.target;
    formData[id] = isEmpty(value) ? '' : value.trim();
    this.setState({ formData });
    this.validateField(id, value);
  };

  validate = evt => {
    const { formData } = this.state;
    const { id, value } = evt.target;
    formData[id] = value;
    this.setState({ formData });
    this.validateField(id, value);
  };

  validateField = (key, value) => {
    let { errors } = this.state;
    if (isEmpty(value)) {
      this.isValid = false;
      errors[key] = `${keyValue[key]} cannot be empty`;
    } else {
      delete errors[key];
    }
    this.setState({ errors });
  };

  /**
   * validatForm
   *
   * Validates all the fields value
   */
  validatForm = formData => {
    fields.forEach(field => {
      this.validateField(field, formData[field]);
    });
  };

  createProject = evt => {
    evt.preventDefault();
    const { formData } = this.state;
    this.isValid = true;
    this.validatForm(formData);
    if (this.isValid) {
      this.props.params.create(formData);
    }
  };

  closeModal = () => {
    this.setState({ errors: {}, formData: {} });
    this.props.params.handleHide(false);
  };

  render() {
    const { params } = this.props;
    const { errors, formData } = this.state;
    return (
      <Modal
        tag="div"
        showCloseIcon={false}
        className={styles.root}
        isOpen={params.showModal}
      >
        <ModalHeader tag="div" className={styles.header}>
          <span className={styles.title}>
            Create Project
          </span>
          <div role="presentation" className={styles.close} onClick={this.closeModal}>
            <img src={close} alt="close" />
          </div>
        </ModalHeader>
        <ModalBody tag="div" className={styles.content}>
          <section>
            <form className={styles.form} onSubmit={this.createProject}>
              <div className={styles.common}>
                {/* UI code which handles the UI before upload starts */}
                <div className={styles.create}>
                  <div className={styles.desc}>
                    <div className="inp-elm pass-box grid-col-16 al-l-m font-small">
                      <div className="font-medium p-l-r-10">
                        <input
                          id="name"
                          type="text"
                          onChange={this.validate}
                          placeholder="Project name"
                          value={formData.name || ''}
                          onBlur={this.trimFieldValue}
                        />
                      </div>
                      {errors.name &&
                        <span className="color-6">{errors.name}</span>
                      }
                    </div>
                    <div className="inp-elm pass-box grid-col-16 al-l-m font-small">
                      <div className="font-medium p-l-r-10">
                        <input
                          type="text"
                          id="description"
                          onChange={this.validate}
                          onBlur={this.trimFieldValue}
                          placeholder="Project description"
                          value={formData.description || ''}
                        />
                      </div>
                      {errors.description &&
                        <span className="color-6">{errors.description}</span>
                      }
                    </div>
                  </div>
                  <div className="text-center">
                    <button
                      type="submit"
                      onClick={this.createProject}
                      className="btn btn-primary font-wt-600"
                    >
                      Create Project
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </section>
        </ModalBody>
      </Modal>
    );
  }
}

CreateProject.propTypes = {
  params: PropTypes.oneOfType([
    PropTypes.object
  ])
};

CreateProject.defaultProps = {
  params: null
};

export default CreateProject;
