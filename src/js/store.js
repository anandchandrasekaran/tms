import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { createHashHistory as createHistory } from 'history';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';

import sagas from './sagas';
import reducers from './reducers';

export const history = createHistory({ queryKey: false });

const sagaMiddleware = createSagaMiddleware();

const reducer = combineReducers(Object.assign({}, reducers, {
  routing: routerReducer
}));

const middlewares = [];

/* eslint-disable no-underscore-dangle */
const composeEnhancers = typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose;
/* eslint-enable */
/* eslint-disable no-undef */
if ((typeof (ENV) !== 'undefined') && ENV.logDispatcher) {
  middlewares.push(createLogger({ collapsed: true }));
}

/* eslint-enable no-undef */
middlewares.push(sagaMiddleware);
middlewares.push(routerMiddleware(history));

export const store = composeEnhancers(applyMiddleware(...middlewares))(createStore)(reducer);

sagaMiddleware.run(sagas);
export default 'store';
